-- reverse: modify "users" table
ALTER TABLE "users" DROP CONSTRAINT "fk_users_primary_email_address";
-- reverse: modify "user_idp_identities" table
ALTER TABLE "user_idp_identities" DROP CONSTRAINT "fk_users_idp_identities";
-- reverse: modify "user_email_addresses" table
ALTER TABLE "user_email_addresses" DROP CONSTRAINT "fk_users_email_addresses";
-- reverse: modify "token_role_bindings" table
ALTER TABLE "token_role_bindings" DROP CONSTRAINT "fk_tokens_role_bindings", DROP CONSTRAINT "fk_token_role_bindings_role_binding";
-- reverse: modify "role_bindings" table
ALTER TABLE "role_bindings" DROP CONSTRAINT "fk_users_role_bindings", DROP CONSTRAINT "fk_role_bindings_role";
-- reverse: modify "element_attributes" table
ALTER TABLE "element_attributes" DROP CONSTRAINT "fk_elements_attributes";
-- reverse: create index "idx_users_name" to table: "users"
DROP INDEX "idx_users_name";
-- reverse: create "users" table
DROP TABLE "users";
-- reverse: create index "idx_user_idp_identities_sub" to table: "user_idp_identities"
DROP INDEX "idx_user_idp_identities_sub";
-- reverse: create index "idx_user_idp_identities_idp" to table: "user_idp_identities"
DROP INDEX "idx_user_idp_identities_idp";
-- reverse: create "user_idp_identities" table
DROP TABLE "user_idp_identities";
-- reverse: create index "idx_user_id_email_address" to table: "user_email_addresses"
DROP INDEX "idx_user_id_email_address";
-- reverse: create "user_email_addresses" table
DROP TABLE "user_email_addresses";
-- reverse: create "tokens" table
DROP TABLE "tokens";
-- reverse: create "token_role_bindings" table
DROP TABLE "token_role_bindings";
-- reverse: create "services" table
DROP TABLE "services";
-- reverse: create index "idx_roles_value" to table: "roles"
DROP INDEX "idx_roles_value";
-- reverse: create index "idx_roles_name" to table: "roles"
DROP INDEX "idx_roles_name";
-- reverse: create "roles" table
DROP TABLE "roles";
-- reverse: create index "idx_role_binding_role_id_user_id_element_id" to table: "role_bindings"
DROP INDEX "idx_role_binding_role_id_user_id_element_id";
-- reverse: create "role_bindings" table
DROP TABLE "role_bindings";
-- reverse: create index "idx_elements_name" to table: "elements"
DROP INDEX "idx_elements_name";
-- reverse: create "elements" table
DROP TABLE "elements";
-- reverse: create index "idx_element_attributes_name" to table: "element_attributes"
DROP INDEX "idx_element_attributes_name";
-- reverse: create "element_attributes" table
DROP TABLE "element_attributes";
