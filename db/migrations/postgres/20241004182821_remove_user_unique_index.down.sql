-- reverse: create index "idx_users_name" to table: "users"
DROP INDEX "idx_users_name";
-- reverse: drop index "idx_users_name" from table: "users"
CREATE UNIQUE INDEX "idx_users_name" ON "users" ("name");
