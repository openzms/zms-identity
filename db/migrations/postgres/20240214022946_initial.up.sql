-- create "element_attributes" table
CREATE TABLE "element_attributes" (
  "id" uuid NOT NULL,
  "element_id" uuid NOT NULL,
  "name" character varying(128) NOT NULL,
  "value" text NULL,
  "description" text NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_element_attributes_name" to table: "element_attributes"
CREATE INDEX "idx_element_attributes_name" ON "element_attributes" ("name");
-- create "elements" table
CREATE TABLE "elements" (
  "id" uuid NOT NULL,
  "creator_user_id" uuid NOT NULL,
  "name" character varying(256) NOT NULL,
  "html_url" text NULL,
  "description" text NULL,
  "is_public" boolean NULL,
  "created_at" timestamptz NOT NULL,
  "approved_at" timestamptz NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  "enabled" boolean NOT NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_elements_name" to table: "elements"
CREATE UNIQUE INDEX "idx_elements_name" ON "elements" ("name");
-- create "role_bindings" table
CREATE TABLE "role_bindings" (
  "id" uuid NOT NULL,
  "role_id" uuid NOT NULL,
  "user_id" uuid NOT NULL,
  "element_id" uuid NULL,
  "created_at" timestamptz NOT NULL,
  "approved_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_role_binding_role_id_user_id_element_id" to table: "role_bindings"
CREATE UNIQUE INDEX "idx_role_binding_role_id_user_id_element_id" ON "role_bindings" ("role_id", "user_id", "element_id");
-- create "roles" table
CREATE TABLE "roles" (
  "id" uuid NOT NULL,
  "name" text NOT NULL,
  "value" bigint NOT NULL,
  "description" text NOT NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_roles_name" to table: "roles"
CREATE UNIQUE INDEX "idx_roles_name" ON "roles" ("name");
-- create index "idx_roles_value" to table: "roles"
CREATE UNIQUE INDEX "idx_roles_value" ON "roles" ("value");
-- create "services" table
CREATE TABLE "services" (
  "id" uuid NOT NULL,
  "name" text NOT NULL,
  "kind" text NOT NULL,
  "endpoint" text NULL,
  "endpoint_api_uri" text NULL,
  "description" text NULL,
  "version" text NULL,
  "api_version" text NULL,
  "enabled" boolean NOT NULL,
  "created_at" timestamptz NOT NULL,
  "updated_at" timestamptz NULL,
  "heartbeat_at" timestamptz NULL,
  "secret" text NOT NULL,
  PRIMARY KEY ("id")
);
-- create "token_role_bindings" table
CREATE TABLE "token_role_bindings" (
  "token_id" uuid NOT NULL,
  "role_binding_id" uuid NOT NULL,
  PRIMARY KEY ("token_id", "role_binding_id")
);
-- create "tokens" table
CREATE TABLE "tokens" (
  "id" uuid NOT NULL,
  "user_id" text NULL,
  "token" character varying(64) NULL,
  "issued_at" timestamptz NULL,
  "expires_at" timestamptz NULL,
  PRIMARY KEY ("id")
);
-- create "user_email_addresses" table
CREATE TABLE "user_email_addresses" (
  "id" uuid NOT NULL,
  "user_id" uuid NOT NULL,
  "email_address" character varying(320) NOT NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_user_id_email_address" to table: "user_email_addresses"
CREATE UNIQUE INDEX "idx_user_id_email_address" ON "user_email_addresses" ("user_id", "email_address");
-- create "user_idp_identities" table
CREATE TABLE "user_idp_identities" (
  "id" uuid NOT NULL,
  "user_id" uuid NOT NULL,
  "sub" character varying(256) NOT NULL,
  "aud" character varying(256) NULL,
  "iss" character varying(256) NULL,
  "idp" text NOT NULL,
  "email_address" character varying(320) NULL,
  "name" character varying(256) NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_user_idp_identities_idp" to table: "user_idp_identities"
CREATE INDEX "idx_user_idp_identities_idp" ON "user_idp_identities" ("idp");
-- create index "idx_user_idp_identities_sub" to table: "user_idp_identities"
CREATE UNIQUE INDEX "idx_user_idp_identities_sub" ON "user_idp_identities" ("sub");
-- create "users" table
CREATE TABLE "users" (
  "id" uuid NOT NULL,
  "name" character varying(128) NOT NULL,
  "given_name" character varying(128) NULL,
  "family_name" character varying(128) NULL,
  "full_name" character varying(256) NULL,
  "created_at" timestamptz NOT NULL,
  "updated_at" timestamptz NULL,
  "deleted_at" timestamptz NULL,
  "enabled" boolean NOT NULL,
  "password" bytea NULL,
  "primary_email_address_id" uuid NULL,
  PRIMARY KEY ("id")
);
-- create index "idx_users_name" to table: "users"
CREATE UNIQUE INDEX "idx_users_name" ON "users" ("name");
-- modify "element_attributes" table
ALTER TABLE "element_attributes" ADD
 CONSTRAINT "fk_elements_attributes" FOREIGN KEY ("element_id") REFERENCES "elements" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
-- modify "role_bindings" table
ALTER TABLE "role_bindings" ADD
 CONSTRAINT "fk_role_bindings_role" FOREIGN KEY ("role_id") REFERENCES "roles" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION, ADD
 CONSTRAINT "fk_users_role_bindings" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
-- modify "token_role_bindings" table
ALTER TABLE "token_role_bindings" ADD
 CONSTRAINT "fk_token_role_bindings_role_binding" FOREIGN KEY ("role_binding_id") REFERENCES "role_bindings" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION, ADD
 CONSTRAINT "fk_tokens_role_bindings" FOREIGN KEY ("token_id") REFERENCES "tokens" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
-- modify "user_email_addresses" table
ALTER TABLE "user_email_addresses" ADD
 CONSTRAINT "fk_users_email_addresses" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
-- modify "user_idp_identities" table
ALTER TABLE "user_idp_identities" ADD
 CONSTRAINT "fk_users_idp_identities" FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
-- modify "users" table
ALTER TABLE "users" ADD
 CONSTRAINT "fk_users_primary_email_address" FOREIGN KEY ("primary_email_address_id") REFERENCES "user_email_addresses" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;
