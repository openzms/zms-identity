-- drop index "idx_users_name" from table: "users"
DROP INDEX "idx_users_name";
-- create index "idx_users_name" to table: "users"
CREATE INDEX "idx_users_name" ON "users" ("name");
