-- disable the enforcement of foreign-keys constraints
PRAGMA foreign_keys = off;
-- create "new_users" table
CREATE TABLE `new_users` (
  `id` uuid NULL,
  `name` text NOT NULL,
  `given_name` text NULL,
  `family_name` text NULL,
  `full_name` text NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL,
  `deleted_at` datetime NULL,
  `enabled` numeric NOT NULL,
  `password` blob NULL,
  `primary_email_address_id` uuid NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_primary_email_address` FOREIGN KEY (`primary_email_address_id`) REFERENCES `user_email_addresses` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- copy rows from old table "users" to new temporary table "new_users"
INSERT INTO `new_users` (`id`, `name`, `given_name`, `family_name`, `full_name`, `created_at`, `updated_at`, `deleted_at`, `enabled`, `password`, `primary_email_address_id`) SELECT `id`, `name`, `given_name`, `family_name`, `full_name`, `created_at`, `updated_at`, `deleted_at`, `enabled`, `password`, `primary_email_address_id` FROM `users`;
-- drop "users" table after copying rows
DROP TABLE `users`;
-- rename temporary table "new_users" to "users"
ALTER TABLE `new_users` RENAME TO `users`;
-- create index "idx_users_name" to table: "users"
CREATE INDEX `idx_users_name` ON `users` (`name`);
-- enable back the enforcement of foreign-keys constraints
PRAGMA foreign_keys = on;
