-- create "user_email_addresses" table
CREATE TABLE `user_email_addresses` (
  `id` uuid NULL,
  `user_id` uuid NOT NULL,
  `email_address` text NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_email_addresses` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_user_id_email_address" to table: "user_email_addresses"
CREATE UNIQUE INDEX `idx_user_id_email_address` ON `user_email_addresses` (`user_id`, `email_address`);
-- create "tokens" table
CREATE TABLE `tokens` (
  `id` uuid NULL,
  `user_id` text NULL,
  `token` text NULL,
  `issued_at` datetime NULL,
  `expires_at` datetime NULL,
  PRIMARY KEY (`id`)
);
-- create "elements" table
CREATE TABLE `elements` (
  `id` uuid NULL,
  `creator_user_id` uuid NOT NULL,
  `name` text NOT NULL,
  `html_url` text NULL,
  `description` text NULL,
  `is_public` numeric NULL,
  `created_at` datetime NOT NULL,
  `approved_at` datetime NULL,
  `updated_at` datetime NULL,
  `deleted_at` datetime NULL,
  `enabled` numeric NOT NULL,
  PRIMARY KEY (`id`)
);
-- create index "idx_elements_name" to table: "elements"
CREATE UNIQUE INDEX `idx_elements_name` ON `elements` (`name`);
-- create "users" table
CREATE TABLE `users` (
  `id` uuid NULL,
  `name` text NOT NULL,
  `given_name` text NULL,
  `family_name` text NULL,
  `full_name` text NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL,
  `deleted_at` datetime NULL,
  `enabled` numeric NOT NULL,
  `password` blob NULL,
  `primary_email_address_id` uuid NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_primary_email_address` FOREIGN KEY (`primary_email_address_id`) REFERENCES `user_email_addresses` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_users_name" to table: "users"
CREATE UNIQUE INDEX `idx_users_name` ON `users` (`name`);
-- create "roles" table
CREATE TABLE `roles` (
  `id` uuid NULL,
  `name` text NOT NULL,
  `value` integer NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
);
-- create index "idx_roles_value" to table: "roles"
CREATE UNIQUE INDEX `idx_roles_value` ON `roles` (`value`);
-- create index "idx_roles_name" to table: "roles"
CREATE UNIQUE INDEX `idx_roles_name` ON `roles` (`name`);
-- create "role_bindings" table
CREATE TABLE `role_bindings` (
  `id` uuid NULL,
  `role_id` uuid NOT NULL,
  `user_id` uuid NOT NULL,
  `element_id` uuid NULL,
  `created_at` datetime NOT NULL,
  `approved_at` datetime NULL,
  `deleted_at` datetime NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_role_bindings` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_role_bindings_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_role_binding_role_id_user_id_element_id" to table: "role_bindings"
CREATE UNIQUE INDEX `idx_role_binding_role_id_user_id_element_id` ON `role_bindings` (`role_id`, `user_id`, `element_id`);
-- create "token_role_bindings" table
CREATE TABLE `token_role_bindings` (
  `token_id` uuid NULL,
  `role_binding_id` uuid NULL,
  PRIMARY KEY (`token_id`, `role_binding_id`),
  CONSTRAINT `fk_token_role_bindings_role_binding` FOREIGN KEY (`role_binding_id`) REFERENCES `role_bindings` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_tokens_role_bindings` FOREIGN KEY (`token_id`) REFERENCES `tokens` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create "element_attributes" table
CREATE TABLE `element_attributes` (
  `id` uuid NULL,
  `element_id` uuid NOT NULL,
  `name` text NOT NULL,
  `value` text NULL,
  `description` text NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_elements_attributes` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_element_attributes_name" to table: "element_attributes"
CREATE INDEX `idx_element_attributes_name` ON `element_attributes` (`name`);
-- create "services" table
CREATE TABLE `services` (
  `id` uuid NULL,
  `name` text NOT NULL,
  `kind` text NOT NULL,
  `endpoint` text NULL,
  `endpoint_api_uri` text NULL,
  `description` text NULL,
  `version` text NULL,
  `api_version` text NULL,
  `enabled` numeric NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NULL,
  `heartbeat_at` datetime NULL,
  `secret` text NOT NULL,
  PRIMARY KEY (`id`)
);
-- create "user_idp_identities" table
CREATE TABLE `user_idp_identities` (
  `id` uuid NULL,
  `user_id` uuid NOT NULL,
  `sub` text NOT NULL,
  `aud` text NULL,
  `iss` text NULL,
  `idp` text NOT NULL,
  `email_address` text NULL,
  `name` text NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_users_idp_identities` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
);
-- create index "idx_user_idp_identities_idp" to table: "user_idp_identities"
CREATE INDEX `idx_user_idp_identities_idp` ON `user_idp_identities` (`idp`);
-- create index "idx_user_idp_identities_sub" to table: "user_idp_identities"
CREATE UNIQUE INDEX `idx_user_idp_identities_sub` ON `user_idp_identities` (`sub`);
