Upgrading PostgreSQL Databases
==============================

[Upgrading a PostgreSQL database](https://www.postgresql.org/docs/current/pgupgrade.html)
to a new major version (e.g., from `15` to `16`) requires an upgrade
process, although there is significant tooling that eases this process for
containerized PostgreSQL databases in the OpenZMS context.

You can manually update using the process described in the PostgreSQL
documentation above, but there is [an open-source
tool](https://github.com/tianon/docker-postgres-upgrade) with published
images [on the Docker Hub](https://hub.docker.com/r/tianon/postgres-upgrade)
that simplifies the process.  For instance, to upgrade the `zms-identity`
database from version `15` to `16` (assuming you are using the `local-dev`
deploy variant), you should be able to follow a process like this:

Stop your zms-identity service and database:

    cd zms-identity
    docker -f deploy/docker-compose.yml down

Move your current data dir to a version 15-specific location.  It is a good
idea to perform a full backup (at both the file level, and the SQL level
with `pg_dump`) prior to this step!

    mv /zms/zms-identity-postgres-local-dev /zms/zms-identity-postgres-local-dev--pg-15

Manually update your docker compose file to use postgres:16 instead of
postgres:15 for the database container.

Initialize your "new" `zms-identity-postgres-local-dev` data dir with
version 16.  Ensure that your database env settings
(`deploy/zms-identity-postgres-local-dev.env`) remain the same as for the
current version 15 database.

    docker -f deploy/docker-compose.yml up -d zms-identity-postgres-local-dev

*Carefully*: drop the newly-initialized `zms_identity_local_dev` database:

    docker exec -it zms-identity-postgres-local-dev psql -U postgres -c 'drop database zms_identity_local_dev;'

Bring the newly-initialized database down:

    docker compose -f deploy/docker-compose.yml down zms-identity-postgres-local-dev

Perform the upgrade using the relevant 15-to-16 `tianon/postgres-upgrade`
image:

    docker run --rm -v /zms/zms-identity-postgres-local-dev--pg-15/:/var/lib/postgresql/15/data -v /zms/zms-identity-postgres-local-dev/:/var/lib/postgresql/16/data tianon/postgres-upgrade:15-to-16 

*Carefully* verify that there were no errors.  Then bring up the database
service, check its logs for any errors, and perform a `vacuumdb` as
instructed at the end of the pgupgrade tool's execution:

    docker compose -f deploy/docker-compose.yml up -d zms-identity-postgres-local-dev 
    docker compose -f deploy/docker-compose.yml logs zms-identity-postgres-local-dev 
    docker exec -it zms-identity-postgres-local-dev vacuumdb -U postgres --all --analyze-in-stages

At this point, you should manually diff SQL dumps of the your old v15
database and the new v16 database to ensure that the upgrade has completed
with no errors.

Finally, bring the `zms-identity` service back up and check its logs to
verify proper startup:

    docker compose -f deploy/docker-compose.yml up -d zms-identity-local-dev 
    docker compose -f deploy/docker-compose.yml logs zms-identity-local-dev 
