Working With Atlas Migrations
=============================

We use [Atlas](https://atlasgo.io/getting-started/) for database migration
planning and application.  We use the [GORM](https://gorm.io/docs/)
object-relational mapper library to declare our SQL data models and to
interact with the database at runtime.  To integrate GORM and Atlas, we rely
on the [atlas-provider-gorm](https://github.com/ariga/atlas-provider-gorm)
provider to inform Atlas's planning engine of our declared database model
state.

Note: this codebase supports both `postgres` and `sqlite`, so we provide
migrations for both.  This is likely to change in the future, so we do not
recommend `sqlite` for production use.

Note: we store our migrations [in raw
SQL](https://atlasgo.io/atlas-schema/sql), rather than in Atlas's 
[native, declarative HCL](https://atlasgo.io/atlas-schema/hcl) format.
We do this so that we can record the migration plans in raw SQL for
inspection, persistence, and a priori or offline planning---rather than
relying on the planner to dynamically generate a plan at migration time to
drive the database to the right state.

Note: we store our migration directory content in
[golang-migrate](https://github.com/golang-migrate/migrate) format, rather
than in Atlas's native format.  This allows us to store both the up and down
migrations, which may be useful in the future (e.g. to switch to
`golang-migrate` if necessary).  However, Atlas does not ever apply the down
migrations.


Configuration
-------------

Our Atlas configuration is described in [atlas.hcl](file://atlas.hcl).  The
configuration consists of four primary blocks:

  * A GORM external schema declaration for `postgres`
  * A GORM environment configuration for `postgres`
  * A GORM external schema declaration for `sqlite`
  * A GORM environment configuration for `sqlite`

  
Typical Use
-----------

The following commands define a target database via environment variables,
and we will use them in the following examples.  We're assuming a typical
`local_dev` OpenZMS deployment, with a specific instance of the
`zms-identity-postgres-local-dev` service at a given IP:

    export DBHOST=`docker inspect zms-identity-postgres-local-dev -f '{{(index .NetworkSettings.Networks "zms-service-local-dev-net").IPAddress}}'`
    export DBNAME="zms_identity_local_dev"
    export DBURL="postgres://postgres:postgres@${DBHOST}/${DBNAME}?search_path=public&sslmode=disable"


Checking Migration Status
-------------------------

You can check the state of your live database (`--url "${DBURL}"`) as
follows.  Note the `--env gorm` arguments: these refer to the Atlas
configuration file, and define default variable values for the `postgres`
migration variants, including the path to the migrations
([migrations/postgres](file://migrations/postgres)).

    atlas migrate status --env gorm --url "${DBURL}"

This should output similarly to the following:

```
Migration Status: OK
  -- Current Version: 20240214022946
  -- Next Version:    Already at latest version
  -- Executed Files:  1
  -- Pending Files:   0
```

Checking Database Status
------------------------

If you have been manually modifying your database, you may want to check if
it differs from the current set of migrations:

    atlas schema diff --env gorm \
        --from "file://migrations/postgres?format=golang-migrate" \
        --to "${DBURL}" \
        --exclude "atlas_schema_revisions"

If your database is identical to the migration state, you will see output
like

```
Schemas are synced, no changes to be made.
```

(Note that we explicitly excluded the `atlas_schema_revisions` table in the
above command, since that is automatically created and maintained by `atlas
migrate`; however, when we use the `schema` subcommand and either the
migrations dir or the current model via gorm as the source (`--from`), this
table is not present in either of those sources, and so will appear here as
a diff.)


Applying Migrations
-------------------

Apply all newer, pending migrations:

    atlas migrate apply --env gorm --url "${DBURL}"


Generating a New Migration
--------------------------

When you change the GORM model schema in the code, you will need to generate
a new migration, so that other users can safely use your changes.  You
should avoid destructive migrations, such as narrowing field conversions and
table or column removal of semantically-relevant data.

Generate a new migration based on the difference between the current set of
migrations, and the current GORM model in the code:

    atlas migrate diff --env gorm foo

(or for `sqlite`:)

    atlas migrate diff --env gorm-sqlite foo

If there are differences, this will generate a `postgres` (or `sqlite`) "up"
migration in `migrations/postgres/<datetime>_foo.up.sql` (or
`migrations/sqlite/<datetime>_foo.up.sql`, and a corresponding "down"
`<datetime>_foo.down.sql` migration.  Remember, Atlas does not use the
"down" migration during rollback to prior versions.

In this codebase, you must generate migrations for both `postgres` and
`sqlite`, since the SQL features used are currently simple and portable.


Editing a New Migration
-----------------------

*You should never edit an old migration that others have used (e.g., that
has been committed to `main` or `dev`.*

If you find that Atlas has missed a particular change, meaning that a new
aspect of the model in the code is not present or incorrect in the "up"
portion of your new migration, you may need to edit it.  If you disagree
with Atlas's migration plan, or need to add a data migration aspect, you
will need to recalculate migration checksums:

    atlas migrate hash --env gorm

(or for `sqlite`):

    atlas migrate hash --env gorm-sqlite


Checking your Migration
-----------------------

You can analyze the latest migration via

    atlas migrate lint --env gorm --latest 1


Rolling Back
------------

If you need to undo an applied migration, you can apply an old version:

    atlas schema apply \
        --url "${DBURL}" \
        --to "file://migrations/postgres?format=golang-migrate&version=20240214022946" \
        --dev-url "docker://postgres/16/dev?search_path=public" \
        --exclude=atlas_schema_revisions

Note that in this command, we *do not* use the `--env gorm` argument.
Instead, we are specific to accomplish exactly what we want.  The migration
plan Atlas presents to you should be carefully verified --- it may match the
generated "down" migration, or it may not.

Once you are satisfied, you can force the current migration status in the
`atlas_schema_revisions` table to match the version you applied above:

    atlas migrate set 20240214022946 --env gorm --url "${DBURL}"

Verify your migration status is correct:

    atlas migrate status --env gorm --url "${DBURL}"



Applying Initial Migrations to a Existing Database
--------------------------------------------------

You can forcibly apply the current model schema as defined in the code via

    atlas schema apply --env gorm --url "${DBURL}" --dry-run

If your schema is old (e.g. from a pre-migration version of this software),
carefully analyze the migration plan presented to you!  Back up your
database first using `pg_dump` and/or `pg_dumpall`!  Once the schema is
applied, however, you can set the current migration status manually like

    atlas migrate set 20240214022946 --env gorm --url "${DBURL}"

If your database is in synch with the schema, and truly has no
`atlas_schema_revisions` table, this should produce the output

```
Schema is synced, no changes to be made
```

If your database is in synch *and* you have an `atlas_schema_revisions`
table, Atlas will try to drop that table, since technically it is maintained
only by the `atlas migrate` subcommand.  You can avoid that by adding
`--exclude atlas_schema_revisions`:

    atlas schema apply --env gorm --url "${DBURL}" --dry-run \
        --exclude atlas_schema_revisions
