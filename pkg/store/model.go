// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"encoding/json"
	"sort"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	Id                    uuid.UUID          `gorm:"primaryKey;type:uuid" json:"id"`
	Name                  string             `gorm:"not null;index;size:128" json:"name" binding:"required,max=128"`
	GivenName             *string            `gorm:"size:128" json:"given_name" binding:"omitempty,max=128"`
	FamilyName            *string            `gorm:"size:128" json:"family_name" binding:"omitempty,max=128"`
	FullName              *string            `gorm:"size:256" json:"full_name" binding:"omitempty,max=256"`
	CreatedAt             time.Time          `gorm:"not null;autoCreateTime" json:"created_at"`
	UpdatedAt             *time.Time         `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt             *time.Time         `json:"deleted_at"`
	Enabled               bool               `gorm:"not null" json:"enabled"`
	Password              []byte             `gorm:"size:32" json:"password" binding:"omitempty,max=32"`
	PrimaryEmailAddressId *uuid.UUID         `gorm:"type:uuid" json:"primary_email_address_id"`
	PrimaryEmailAddress   *UserEmailAddress  `gorm:"foreignKey:PrimaryEmailAddressId" json:"primary_email_address"`
	EmailAddresses        []UserEmailAddress `json:"email_addresses"`
	IdpIdentities         []UserIdpIdentity  `json:"idp_identities"`
	RoleBindings          []RoleBinding      `json:"role_bindings"`
}

func (x User) MarshalJSON() ([]byte, error) {
	type user User
	xn := user(x)
	xn.Password = nil
	return json.Marshal(xn)
}

func (x *User) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

func (u *User) SortRoleBindings() {
	if u.RoleBindings == nil || len(u.RoleBindings) == 0 {
		return
	}

	//
	// NB: sort the role bindings in descending order, so that policy
	// middleware will match the most encompassing scope first.
	//
	sort.SliceStable(u.RoleBindings, func(i, j int) bool {
		if u.RoleBindings[i].Role == nil {
			return true
		} else if u.RoleBindings[j].Role == nil {
			return false
		} else if u.RoleBindings[i].Role.Value == u.RoleBindings[j].Role.Value {
			if u.RoleBindings[i].ElementId == nil {
				return false
			} else if u.RoleBindings[j].ElementId == nil {
				return true
			} else {
				return u.RoleBindings[i].ElementId.String() >= u.RoleBindings[j].ElementId.String()
			}
		} else {
			return u.RoleBindings[i].Role.Value >= u.RoleBindings[j].Role.Value
		}
	})
}

type UserEmailAddress struct {
	Id           uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	UserId       uuid.UUID `gorm:"foreignKey:UserId;not null;uniqueIndex:idx_user_id_email_address" json:"user_id"`
	EmailAddress string    `gorm:"uniqueIndex:idx_user_id_email_address;not null;size:320" json:"email_address" binding:"max=320"`
}

func (x *UserEmailAddress) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type UserIdpIdentity struct {
	Id           uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	UserId       uuid.UUID `gorm:"foreignKey:UserId;not null" json:"user_id"`
	Sub          string    `gorm:"not null;uniqueIndex;size:256" json:"sub" binding:"required,max=256"`
	Aud          string    `gorm:"size:256" json:"aud" binding:"omitempty,max=256"`
	Iss          string    `gorm:"size:256" json:"iss" binding:"omitempty,max=256"`
	Idp          string    `gorm:"not null;index" json:"idp"`
	EmailAddress string    `gorm:"size:320" json:"email_address" binding:"required,max=320"`
	Name         string    `gorm:"size:256" json:"name" binding:"omitempty,max=256"`
}

func (x *UserIdpIdentity) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Element struct {
	Id            uuid.UUID          `gorm:"primaryKey;type:uuid" json:"id"`
	CreatorUserId uuid.UUID          `gorm:"foreignKey:UserId;not null;type:uuid" json:"creator_user_id"`
	Name          string             `gorm:"size:256;uniqueIndex;not null" json:"name" binding:"required"`
	HtmlUrl       string             `json:"html_url"`
	Description   *string            `json:"description"`
	IsPublic      bool               `json:"is_public"`
	CreatedAt     time.Time          `gorm:"not null;autoCreateTime" json:"created_at"`
	ApprovedAt    *time.Time         `json:"approved_at"`
	UpdatedAt     *time.Time         `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt     *time.Time         `json:"deleted_at"`
	Enabled       bool               `gorm:"not null" json:"enabled"`
	Attributes    []ElementAttribute `json:"attributes"`
}

func (x *Element) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type ElementAttribute struct {
	Id          uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId   uuid.UUID `gorm:"foreignKey:ElementId;not null;type:uuid" json:"element_id"`
	Name        string    `gorm:"not null;size:128;index" json:"name"`
	Value       *string   `json:"value"`
	Description *string   `json:"description"`
}

func (x *ElementAttribute) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Role struct {
	Id          uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	Name        string    `gorm:"not null;uniqueIndex" json:"name"`
	Value       int       `gorm:"not null;uniqueIndex" json:"value"`
	Description string    `gorm:"not null" json:"description"`
}

func (x *Role) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type RoleBinding struct {
	Id         uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	RoleId     uuid.UUID  `gorm:"foreignKey:RoleId;type:uuid;not null;uniqueIndex:idx_role_binding_role_id_user_id_element_id" json:"role_id" binding:"required"`
	UserId     uuid.UUID  `gorm:"foreignKey:UserId;type:uuid;not null;uniqueIndex:idx_role_binding_role_id_user_id_element_id" json:"user_id" binding:"required"`
	ElementId  *uuid.UUID `gorm:"foreignKey:ElementId;type:uuid;uniqueIndex:idx_role_binding_role_id_user_id_element_id" json:"element_id"`
	CreatedAt  time.Time  `gorm:"not null;autoCreateTime" json:"created_at"`
	ApprovedAt *time.Time `json:"approved_at"`
	DeletedAt  *time.Time `json:"deleted_at"`
	Role       *Role      `json:"role"`
}

func (x *RoleBinding) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Token struct {
	Id        uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	UserId    uuid.UUID  `gorm:"foreignKey:UserId" json:"user_id"`
	Token     string     `gorm:"uniqueKey;size:64" json:"token"`
	IssuedAt  time.Time  `gorm:"not null,autoCreateTime" json:"issued_at"`
	ExpiresAt *time.Time `json:"expires_at"`
	RoleBindings []TokenRoleBinding `json:"role_bindings"`
}

func (x *Token) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

func (t *Token) GetElementIds() (uuids []*uuid.UUID) {
	uuids = nil
	for _, trb := range t.RoleBindings {
		if trb.RoleBinding == nil {
			continue
		}
		if trb.RoleBinding.ElementId == nil {
			continue
		}
		if uuids == nil {
			uuids = make([]*uuid.UUID, 0, len(t.RoleBindings))
		}
		uuids = append(uuids, trb.RoleBinding.ElementId)
	}

	return uuids
}

func (t *Token) SortRoleBindings() {
	if t.RoleBindings == nil || len(t.RoleBindings) == 0 {
		return
	}

	//
	// NB: sort the role bindings in descending order, so that policy
	// middleware will match the most encompassing scope first.
	//
	sort.SliceStable(t.RoleBindings, func(i, j int) bool {
		if t.RoleBindings[i].RoleBinding == nil || t.RoleBindings[i].RoleBinding.Role == nil {
			return true
		} else if t.RoleBindings[j].RoleBinding == nil || t.RoleBindings[j].RoleBinding.Role == nil {
			return false
		} else if t.RoleBindings[i].RoleBinding.Role.Value == t.RoleBindings[j].RoleBinding.Role.Value {
			if t.RoleBindings[i].RoleBinding.ElementId == nil {
				return false
			} else if t.RoleBindings[j].RoleBinding.ElementId == nil {
				return true
			} else {
				return t.RoleBindings[i].RoleBinding.ElementId.String() >= t.RoleBindings[j].RoleBinding.ElementId.String()
			}
		} else {
			return t.RoleBindings[i].RoleBinding.Role.Value >= t.RoleBindings[j].RoleBinding.Role.Value
		}
	})
}

func (x *Token) BeforeDelete(tx *gorm.DB) (err error) {
	tx.Delete(&TokenRoleBinding{}, "token_id = ?", x.Id.String())
	return nil
}

type TokenRoleBinding struct {
	TokenId       uuid.UUID   `gorm:"primaryKey;type:uuid;foreignKey:TokenId" json:"token_id"`
	RoleBindingId uuid.UUID   `gorm:"primaryKey;type:uuid;foreignKey:RoleBindingId,not null" json:"role_binding_id"`
	RoleBinding   *RoleBinding `json:"role_binding"`
}

func (x *Service) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Service struct {
	Id             uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	Name           string     `gorm:"not null" json:"name"`
	Kind           string     `gorm:"not null" json:"kind"`
	Endpoint       string     `gorm:"uniqueKey:service_endpoint_key" json:"endpoint"`
	EndpointApiUri string     `json:"endpoint_api_uri"`
	Description    string     `json:"description"`
	Version        string     `json:"version"`
	ApiVersion     string     `json:"api_version"`
	Enabled        bool       `gorm:"not null" json:"enabled"`
	CreatedAt      time.Time  `gorm:"not null;autoCreateTime" json:"created_at"`
	UpdatedAt      *time.Time `gorm:"autoUpdateTime" json:"updated_at"`
	HeartbeatAt    *time.Time `json:"heartbeat_at"`
	Secret         string     `gorm:"not null" json:"-"`
}
