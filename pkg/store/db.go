// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"fmt"
	"reflect"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/token"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/policy"

	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/config"
	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/version"
)

func GetDatabase(serverConfig *config.Config, dbConfig *gorm.Config) (db *gorm.DB, err error) {
	var localConfig gorm.Config
	if dbConfig != nil {
		localConfig = *dbConfig
	}
	localConfig.NowFunc = func() time.Time { return time.Now().UTC() }
	localConfig.DisableForeignKeyConstraintWhenMigrating = true
	switch serverConfig.DbDriver {
	case "sqlite":
		db, err = gorm.Open(sqlite.Open(serverConfig.DbDsn), &localConfig)
	case "postgres":
		db, err = gorm.Open(postgres.Open(serverConfig.DbDsn), &localConfig)
	default:
		db, err = nil, errors.New("unsupported database driver")
	}
	return db, err
}

func InitDatabase(serverConfig *config.Config, db *gorm.DB) (bootstrapElementId *uuid.UUID,bootstrapUserId *uuid.UUID, err error) {
	if serverConfig.DbGormMigrate {
		if err := MigrateDatabase(serverConfig, db); err != nil {
			return nil, nil, err
		}
	}
	if serverConfig.Bootstrap {
		bootstrapElementId, bootstrapUserId, err = BootstrapDatabase(serverConfig, db)
	} else {
		var bootstrapUser User
		var bootstrapElement Element
		if res := db.Where(&User{Name: serverConfig.BootstrapUser, Enabled: true}).First(&bootstrapUser); res.Error != nil {
			return nil, nil, res.Error
		}
		if res := db.Where(&Element{Name: serverConfig.BootstrapElementName, Enabled: true}).First(&bootstrapElement); res.Error != nil {
			return nil, nil, res.Error
		}
		bootstrapElementId = &bootstrapElement.Id
		bootstrapUserId = &bootstrapUser.Id
	}
	return bootstrapElementId, bootstrapUserId, err
}

func MigrateDatabase(serverConfig *config.Config, db *gorm.DB) error {
	return db.AutoMigrate(
		&User{}, &UserEmailAddress{}, &UserIdpIdentity{},
		&Element{}, &ElementAttribute{},
		&Token{}, &TokenRoleBinding{}, &Role{}, &RoleBinding{},
		&Service{},
	)
}

func BootstrapDatabase(serverConfig *config.Config, db *gorm.DB) (bootstrapElementId *uuid.UUID, bootstrapUserId *uuid.UUID, err error) {
	// Checks
	if serverConfig.BootstrapToken != "" {
		err := token.Validate(serverConfig.BootstrapToken)
		if err != nil {
			return nil, nil, fmt.Errorf("invalid bootstrap token: %w", err)
		}
	}

	tx := db.Begin()
	defer func() {
		if er := recover(); er != nil {
			log.Error().Msg(fmt.Sprintf("bootstrap error: %+v", er))
			tx.Rollback()
		}
	}()

	if serverConfig.ServiceId != "" && serverConfig.ServiceName != "" {
		var serviceId uuid.UUID
		if serviceIdParsed, err := uuid.Parse(serverConfig.ServiceId); err != nil {
			return nil, nil, fmt.Errorf("invalid ServiceId %s", serverConfig.ServiceId)
		} else {
			serviceId = serviceIdParsed
		}

		service := &Service{
			Id: serviceId,
			Name: serverConfig.ServiceName,
			Kind: "identity",
			Endpoint: serverConfig.RpcEndpoint,
			EndpointApiUri: serverConfig.HttpEndpoint,
			Description: "OpenZMS identity service",
			Version: version.VersionString,
			ApiVersion: "v1",
			Enabled: true,
		}
		if res := tx.Where(&Service{Id: service.Id}).FirstOrCreate(&service); res.Error != nil {
			return nil, nil, res.Error
		}
	}

	var adminRole Role
	var ownerRole Role
	for _, r := range policy.DefaultRoles {
		role := Role{Name: r.Name, Value: r.Value, Description: r.Description}
		if res := tx.Where(&Role{Name: r.Name}).Attrs(&Role{Description: r.Description, Value: r.Value}).FirstOrCreate(&role); res.Error != nil {
			return nil, nil, res.Error
		}
		if role.Name == "admin" {
			adminRole = role
		} else if role.Name == "owner" {
			ownerRole = role
		}
	}

	var passwordHash []byte
	var hperr error
	if serverConfig.BootstrapPassword != "" {
		passwordHash, hperr = bcrypt.GenerateFromPassword([]byte(serverConfig.BootstrapPassword), bcrypt.DefaultCost)
		if hperr != nil {
			return nil, nil, fmt.Errorf("invalid password: %w", hperr)
		}
	}

	var userElementPasswordHash []byte
	if serverConfig.BootstrapUserElementPassword != "" {
		userElementPasswordHash, hperr = bcrypt.GenerateFromPassword([]byte(serverConfig.BootstrapUserElementPassword), bcrypt.DefaultCost)
		if hperr != nil {
			return nil, nil, fmt.Errorf("invalid password: %w", hperr)
		}
	}

	// Ensure bootstrap admin user
	var adminUser *User
	if serverConfig.BootstrapUser != "" && serverConfig.BootstrapEmail != "" {
		var user User
		var email UserEmailAddress
		if res := tx.Where(&User{Name: serverConfig.BootstrapUser, Enabled: true}).FirstOrCreate(&user); res.Error != nil {
			return nil, nil, res.Error
		}
		if res := tx.Where(&UserEmailAddress{UserId: user.Id, EmailAddress: serverConfig.BootstrapEmail}).FirstOrCreate(&email); res.Error != nil {
			return nil, nil, res.Error
		}
		if user.PrimaryEmailAddressId == nil || email.Id != *user.PrimaryEmailAddressId {
			user.PrimaryEmailAddressId = &email.Id
			if res := tx.Save(&user); res.Error != nil {
				return nil, nil, res.Error
			}
		}
		//db.Where(&UserEmailAddress{UserId: user.Id}).
		//	Not("email_address = ?", serverConfig.BootstrapEmail).
		//	Updates(map[string]interface{}{"is_primary": false})

		var adminRoleBinding RoleBinding
		if res := tx.Where(&RoleBinding{UserId: user.Id, RoleId: adminRole.Id}).FirstOrCreate(&adminRoleBinding); res.Error != nil {
			return nil, nil, res.Error
		}
		if adminRoleBinding.ApprovedAt == nil {
			t := time.Now().UTC()
			adminRoleBinding.ApprovedAt = &t
			if res := tx.Save(&adminRoleBinding); res.Error != nil {
				return nil, nil, res.Error
			}
		}

		if serverConfig.BootstrapPassword != "" && !reflect.DeepEqual(user.Password, passwordHash) {
			user.Password = passwordHash
			if res := tx.Save(user); res.Error != nil {
				return nil, nil, res.Error
			}
		}

		adminUser = &user
		bootstrapUserId = &user.Id

		log.Debug().Msg(fmt.Sprintf("admin user: %+v", adminUser))

		var adminElementRoleBinding RoleBinding
		var adminElement Element
		if serverConfig.BootstrapElementName != "" {
			if res := tx.Where(&Element{Name: serverConfig.BootstrapElementName, CreatorUserId: user.Id, Enabled: true}).FirstOrCreate(&adminElement); res.Error != nil {
				return nil, nil, res.Error
			}
			if adminElement.ApprovedAt == nil {
				t := time.Now().UTC()
				adminElement.ApprovedAt = &t
				if res := tx.Save(adminElement); res.Error != nil {
					return nil, nil, res.Error
				}
			}

			bootstrapElementId = &adminElement.Id
			log.Debug().Msg(fmt.Sprintf("admin element: %+v", adminElement))

			if res := tx.Where(&RoleBinding{UserId: user.Id, ElementId: &adminElement.Id, RoleId: ownerRole.Id}).FirstOrCreate(&adminElementRoleBinding); res.Error != nil {
				return nil, nil, res.Error
			}
			if adminElementRoleBinding.ApprovedAt == nil {
				t := time.Now().UTC()
				adminElementRoleBinding.ApprovedAt = &t
				if res := tx.Save(&adminElementRoleBinding); res.Error != nil {
					return nil, nil, res.Error
				}
			}
		}

		if serverConfig.BootstrapToken != "" {
			var tok Token
			if res := tx.Where(&Token{UserId: user.Id, Token: serverConfig.BootstrapToken, ExpiresAt: nil}).FirstOrCreate(&tok); res.Error != nil {
				return nil, nil, res.Error
			}
			if res := tx.Where(&TokenRoleBinding{TokenId: tok.Id, RoleBindingId: adminRoleBinding.Id}).FirstOrCreate(&TokenRoleBinding{}); res.Error != nil {
				return nil, nil, res.Error
			}
			if adminElementRoleBinding.Id != uuid.Nil {
				if res := tx.Where(&TokenRoleBinding{TokenId: tok.Id, RoleBindingId: adminElementRoleBinding.Id}).FirstOrCreate(&TokenRoleBinding{}); res.Error != nil {
					return nil, nil, res.Error
				}
			}
		}
	}

	if adminUser != nil && serverConfig.BootstrapUserElementName != "" {
		var userElement Element
		if res := tx.Where(&Element{Name: serverConfig.BootstrapUserElementName, HtmlUrl: serverConfig.BootstrapUserElementHtmlUrl, CreatorUserId: adminUser.Id, Enabled: true}).FirstOrCreate(&userElement); res.Error != nil {
			return nil, nil, res.Error
		}
		if userElement.ApprovedAt == nil {
			t := time.Now().UTC()
			userElement.ApprovedAt = &t
			if res := tx.Save(userElement); res.Error != nil {
				return nil, nil, res.Error
			}
		}

		if serverConfig.BootstrapUserElementUser != "" && serverConfig.BootstrapUserElementEmail != "" {
			var user User
			var email UserEmailAddress
			if res := tx.Where(&User{Name: serverConfig.BootstrapUserElementUser, Enabled: true}).FirstOrCreate(&user); res.Error != nil {
				return nil, nil, res.Error
			}
			if res := tx.Where(&UserEmailAddress{UserId: user.Id, EmailAddress: serverConfig.BootstrapUserElementEmail}).FirstOrCreate(&email); res.Error != nil {
				return nil, nil, res.Error
			}
			if user.PrimaryEmailAddressId == nil || email.Id != *user.PrimaryEmailAddressId {
				user.PrimaryEmailAddressId = &email.Id
				if res := tx.Save(&user); res.Error != nil {
					return nil, nil, res.Error
				}
			}
			//db.Where(&UserEmailAddress{UserId: user.Id}).
			//	Not("email_address = ?", serverConfig.BootstrapEmail).
			//	Updates(map[string]interface{}{"is_primary": false})

			var roleBinding RoleBinding
			if res := tx.Where(&RoleBinding{UserId: user.Id, ElementId: &userElement.Id, RoleId: ownerRole.Id}).FirstOrCreate(&roleBinding); res.Error != nil {
				return nil, nil, res.Error
			}
			if roleBinding.ApprovedAt == nil {
				t := time.Now().UTC()
				roleBinding.ApprovedAt = &t
				if res := tx.Save(&roleBinding); res.Error != nil {
					return nil, nil, res.Error
				}
			}

			if serverConfig.BootstrapUserElementPassword != "" && !reflect.DeepEqual(user.Password, userElementPasswordHash) {
				user.Password = userElementPasswordHash
				if res := tx.Save(user); res.Error != nil {
					return nil, nil, res.Error
				}
			}

			if serverConfig.BootstrapUserElementToken != "" {
				var tok Token
				if res := tx.Where(&Token{UserId: user.Id, Token: serverConfig.BootstrapUserElementToken, ExpiresAt: nil}).FirstOrCreate(&tok); res.Error != nil {
					return nil, nil, res.Error
				}
				if res := tx.Where(&TokenRoleBinding{TokenId: tok.Id, RoleBindingId: roleBinding.Id}).FirstOrCreate(&TokenRoleBinding{}); res.Error != nil {
					return nil, nil, res.Error
				}
			}
		}
	}

	if res := tx.Commit(); res.Error == nil {
		log.Debug().Msg(fmt.Sprintf("bootstrap: element=%+v, user=%+v", bootstrapElementId, bootstrapUserId))
		return bootstrapElementId, bootstrapUserId, nil
	} else {
		log.Error().Msg(fmt.Sprintf("bootstrap failure: %+v", res.Error))
		return nil, nil, res.Error
	}
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func MakeILikeClause(config *config.Config, field string) string {
	if config.DbDriver == "postgres" {
		return field + " ilike ?"
	} else {
		return field + " like ?"
	}
}
