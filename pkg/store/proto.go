// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"google.golang.org/protobuf/types/known/timestamppb"

	identity_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
)

// NB: caller must Preload any relations; elaborate parameter only controls
// output of them, to give caller more flexibility.

func (in *UserEmailAddress) ToProto(out *identity_v1.UserEmailAddress, elaborate bool) error {
	out.Id = in.Id.String()
	out.UserId = in.UserId.String()
	out.EmailAddress = in.EmailAddress
	return nil
}

func (in *User) ToProto(out *identity_v1.User, elaborate bool) error {
	out.Id = in.Id.String()
	out.Name = in.Name
	out.GivenName = in.GivenName
	out.FamilyName = in.FamilyName
	out.FullName = in.FullName
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	out.Enabled = in.Enabled
	out.PrimaryEmailAddressId = in.PrimaryEmailAddressId.String()
	if elaborate && in.PrimaryEmailAddress != nil {
		out.PrimaryEmailAddress = &identity_v1.UserEmailAddress{}
		in.PrimaryEmailAddress.ToProto(out.PrimaryEmailAddress, elaborate)
	}
	if elaborate {
		for _, x := range in.EmailAddresses {
			outX := identity_v1.UserEmailAddress{}
			x.ToProto(&outX, elaborate)
			out.EmailAddresses = append(out.EmailAddresses, &outX)
		}
		for _, x := range in.RoleBindings {
			outX := identity_v1.RoleBinding{}
			x.ToProto(&outX, elaborate)
			out.RoleBindings = append(out.RoleBindings, &outX)
		}
	}
	return nil
}

func (in *Role) ToProto(out *identity_v1.Role, elaborate bool) error {
	out.Id = in.Id.String()
	out.Name = in.Name
	out.Value = int32(in.Value)
	out.Description = in.Description
	return nil
}

func (in *RoleBinding) ToProto(out *identity_v1.RoleBinding, elaborate bool) error {
	includeInvalidBindings := false
	if !includeInvalidBindings {
		if in.ApprovedAt == nil || in.DeletedAt != nil {
			return nil
		}
	}

	out.Id = in.Id.String()
	out.RoleId = in.RoleId.String()
	out.UserId = in.UserId.String()
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.ElementId != nil {
		s := in.ElementId.String()
		out.ElementId = &s
	}
	if in.ApprovedAt != nil {
		out.ApprovedAt = timestamppb.New(*in.ApprovedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}

	return nil
}

func (in *Token) ToProto(out *identity_v1.Token, elaborate bool) error {
	out.Id = in.Id.String()
	out.UserId = in.UserId.String()
	out.Token = in.Token
	out.IssuedAt = timestamppb.New(in.IssuedAt)
	if in.ExpiresAt != nil {
		out.ExpiresAt = timestamppb.New(*in.ExpiresAt)
	}

	for _, rb := range in.RoleBindings {
		if rb.RoleBinding == nil {
			log.Warn().Msg(fmt.Sprintf("unexpected nil RoleBinding in TokenRoleBinding.Id %+v (likely db inconsistency)", rb.RoleBindingId))
			continue
		}
		prb := identity_v1.RoleBinding{}
		if err := rb.RoleBinding.ToProto(&prb, elaborate); err != nil {
			return err
		} else {
			out.RoleBindings = append(out.RoleBindings, &prb)
		}
	}

	return nil
}

func (in *Element) ToProto(out *identity_v1.Element, elaborate bool) error {
	out.Id = in.Id.String()
	out.CreatorUserId = in.CreatorUserId.String()
	out.Name = in.Name
	out.HtmlUrl = in.HtmlUrl
	out.Description = in.Description
	out.IsPublic = in.IsPublic
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.ApprovedAt != nil {
		out.ApprovedAt = timestamppb.New(*in.ApprovedAt)
	}
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	out.Enabled = in.Enabled
	if elaborate {
		for _, x := range in.Attributes {
			outX := identity_v1.ElementAttribute{}
			x.ToProto(&outX, elaborate)
			out.Attributes = append(out.Attributes, &outX)
		}
	}
	return nil
}

func (in *ElementAttribute) ToProto(out *identity_v1.ElementAttribute, elaborate bool) error {
	out.Id = in.Id.String()
	out.ElementId = in.ElementId.String()
	out.Name = in.Name
	if in.Value != nil {
		out.Value = *in.Value
	}
	if in.Description != nil {
		out.Description = in.Description
	}
	return nil
}

func (in *Service) ToProto(out *identity_v1.Service, elaborate bool) error {
	out.Id = in.Id.String()
	out.Name = in.Name
	out.Kind = in.Kind
	out.Endpoint = in.Endpoint
	out.EndpointApiUri = in.EndpointApiUri
	out.Description = in.Description
	out.Version = in.Version
	out.ApiVersion = in.ApiVersion
	out.Enabled = in.Enabled
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.HeartbeatAt != nil {
		out.HeartbeatAt = timestamppb.New(*in.HeartbeatAt)
	}
	return nil
}
