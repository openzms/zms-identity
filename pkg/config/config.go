// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package config

import (
	"flag"
	"os"
	"strconv"
	"time"
)

type Config struct {
	Verbose                      bool
	Debug                        bool
	HttpEndpointListen           string
	HttpEndpoint                 string
	HealthHttpEndpoint           string
	RpcEndpointListen            string
	RpcEndpoint                  string
	ServiceId                    string
	ServiceName                  string
	DbDriver                     string
	DbDsn                        string
	DbGormMigrate                bool
	Bootstrap                    bool
	BootstrapUser                string
	BootstrapPassword            string
	BootstrapEmail               string
	BootstrapToken               string
	BootstrapElementName         string
	BootstrapUserElementName     string
	BootstrapUserElementHtmlUrl  string
	BootstrapUserElementUser     string
	BootstrapUserElementPassword string
	BootstrapUserElementEmail    string
	BootstrapUserElementToken    string
	UserTokenDuration            time.Duration
	PersonalAccessTokenDuration  time.Duration
	AutoAccountApproval          string
}

func getEnvBool(name string, defValue bool) bool {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseBool(val); err != nil {
			pval = false
		} else {
			return pval
		}
	}
	return defValue
}

func getEnvString(name string, defValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	return defValue
}

func getEnvDuration(name string, defValue time.Duration) time.Duration {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := time.ParseDuration(val); err != nil {
			return defValue
		} else {
			return pval
		}
	}
	return defValue
}

func LoadConfig() *Config {
	dd, _ := time.ParseDuration("24h")

	var config Config
	flag.BoolVar(&config.Debug, "debug",
		getEnvBool("LOG_DEBUG", false), "Enable debug logging.")
	flag.BoolVar(&config.Verbose, "verbose",
		getEnvBool("LOG_VERBOSE", false), "Enable verbose logging.")

	flag.StringVar(&config.HttpEndpointListen, "http-endpoint-listen",
		getEnvString("HTTP_ENDPOINT_LISTEN", "0.0.0.0:8000"), "HTTP endpoint bind address")
	flag.StringVar(&config.HttpEndpoint, "http-endpoint",
		getEnvString("HTTP_ENDPOINT", "0.0.0.0:8000"), "HTTP endpoint external address")
	flag.StringVar(&config.HealthHttpEndpoint, "health-http-endpoint",
		getEnvString("HEALTH_HTTP_ENDPOINT", "0.0.0.0:8001"), "HTTP health endpoint address")
	flag.StringVar(&config.RpcEndpointListen, "rpc-endpoint-listen",
		getEnvString("RPC_ENDPOINT_LISTEN", "0.0.0.0:8002"), "RPC endpoint bind address")
	flag.StringVar(&config.RpcEndpoint, "rpc-endpoint",
		getEnvString("RPC_ENDPOINT", "0.0.0.0:8002"), "RPC endpoint external address")
	flag.StringVar(&config.ServiceId, "service-id",
		getEnvString("SERVICE_ID", "33453677-4573-4b4a-b006-8fd073220001"), "Service ID")
	flag.StringVar(&config.ServiceName, "service-name",
		getEnvString("SERVICE_NAME", "identity"), "Service Name")
	flag.StringVar(&config.DbDriver, "db-driver",
		getEnvString("DB_DRIVER", "sqlite"), "Database driver (sqlite, postgres)")
	flag.StringVar(&config.DbDsn, "db-dsn",
		getEnvString("DB_DSN", "file::memory:?cache=shared"), "Database DSN")
	flag.BoolVar(&config.DbGormMigrate, "db-gorm-migrate",
		getEnvBool("DB_GORM_MIGRATE", false), "Enable/disable GORM automatic database migration.  WARNING: you should never do this.  Use Atlas migrations!")
	flag.BoolVar(&config.Bootstrap, "bootstrap",
		getEnvBool("BOOTSTRAP", false), "Enable/disable database bootstrap")
	flag.StringVar(&config.BootstrapUser, "bootstrap-user",
		getEnvString("BOOTSTRAP_USER", "admin"), "Bootstrap admin username")
	flag.StringVar(&config.BootstrapPassword, "bootstrap-password",
		getEnvString("BOOTSTRAP_PASSWORD", ""), "Bootstrap admin user password")
	flag.StringVar(&config.BootstrapEmail, "bootstrap-email",
		getEnvString("BOOTSTRAP_EMAIL", "root@localhost"), "Bootstrap admin user email address")
	flag.StringVar(&config.BootstrapToken, "bootstrap-token",
		getEnvString("BOOTSTRAP_TOKEN", ""), "Bootstrap admin user admin token")
	flag.StringVar(&config.BootstrapElementName, "bootstrap-element-name",
		getEnvString("BOOTSTRAP_ELEMENT_NAME", "admin"), "Bootstrap admin element name")
	flag.StringVar(&config.BootstrapUserElementName, "bootstrap-user-element-name",
		getEnvString("BOOTSTRAP_USER_ELEMENT_NAME", ""), "Bootstrap user element name")
	flag.StringVar(&config.BootstrapUserElementHtmlUrl, "bootstrap-user-element-html-url",
		getEnvString("BOOTSTRAP_USER_ELEMENT_HTML_URL", ""), "Bootstrap user element html url")
	flag.StringVar(&config.BootstrapUserElementUser, "bootstrap-user-element-user",
		getEnvString("BOOTSTRAP_USER_ELEMENT_USER", ""), "Bootstrap user element username")
	flag.StringVar(&config.BootstrapUserElementPassword, "bootstrap-user-element-password",
		getEnvString("BOOTSTRAP_USER_ELEMENT_PASSWORD", ""), "Bootstrap user element user password")
	flag.StringVar(&config.BootstrapUserElementEmail, "bootstrap-user-element-email",
		getEnvString("BOOTSTRAP_USER_ELEMENT_EMAIL", ""), "Bootstrap user element user email")
	flag.StringVar(&config.BootstrapUserElementToken, "bootstrap-user-element-token",
		getEnvString("BOOTSTRAP_USER_ELEMENT_TOKEN", ""), "Bootstrap user element user token")
	flag.DurationVar(&config.UserTokenDuration, "user-token-duration",
		getEnvDuration("USER_TOKEN_DURATION", dd), "Default user token time to expiration expressed as a golang duration (https://pkg.go.dev/time#ParseDuration)")
	flag.DurationVar(&config.PersonalAccessTokenDuration, "personal-access-token-duration",
		getEnvDuration("PERSONAL_ACCESS_TOKEN_DURATION", dd), "Default personal-access token time to expiration expressed as a golang duration (https://pkg.go.dev/time#ParseDuration)")
	flag.StringVar(&config.AutoAccountApproval, "auto-account-approval",
		getEnvString("AUTO_ACCOUNT_APPROVAL", ""), "By default, we do not automatically approve new accounts (e.g. via SSO).  If you set this value to `account`, new accounts will be automatically granted a RoleAccount rolebinding, allowing them to modify the account, get API tokens, etc.  If you set this value to `viewer`, new users will be granted a global RoleViewer rolebinding, allowing them to see non-confidential element- and user-specific data.")

	flag.Parse()

	return &config
}
