// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package southbound

import (
	"context"
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/peer"
	"google.golang.org/protobuf/types/known/timestamppb"
	"github.com/google/uuid"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"
	event_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
	identity_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/store"
)

func (s *Server) RegisterService(ctx context.Context, req *identity_v1.RegisterServiceRequest) (resp *identity_v1.RegisterServiceResponse, err error) {
	if req == nil || req.Service == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid RegisterServiceRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	log.Info().Msg(fmt.Sprintf("RegisterService(%+v)", req))

	// If the service ID does not exist, we will create it.  If it matches
	// an existing service, we will update that service.
	id := uuid.Nil
	if req.Service.Id != "" {
		if idParsed, err := uuid.Parse(req.Service.Id); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "invalid Service.Id uuid")
		} else {
			id = idParsed
		}
	}

	service := store.Service{Id: id}
	res := s.db.Where(&store.Service{Id: id}).First(&service)

	// Update or initialize the service fields.
	service.Name = req.Service.Name
	service.Kind = req.Service.Kind
	service.Endpoint = req.Service.Endpoint
	service.EndpointApiUri = req.Service.EndpointApiUri
	service.Description = req.Service.Description
	service.Version = req.Service.Version
	service.ApiVersion = req.Service.ApiVersion
	service.Enabled = req.Service.Enabled

	resp = &identity_v1.RegisterServiceResponse{Header: &identity_v1.ResponseHeader{}}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = resp.Header.ReqId
	}
	resp.Service = req.Service

	var eventType int32
	if res.RowsAffected == 1 {
		s.db.Save(&service)
		eventType = int32(event_v1.EventType_ET_UPDATED)
	} else {
		service.Secret = uuid.New().String()
		s.db.Create(&service)
		eventType = int32(event_v1.EventType_ET_CREATED)
	}
	resp.Service.CreatedAt = timestamppb.New(service.CreatedAt)
	if service.UpdatedAt != nil {
		resp.Service.UpdatedAt = timestamppb.New(*service.UpdatedAt)
	}
	if service.HeartbeatAt != nil {
		resp.Service.HeartbeatAt = timestamppb.New(*service.HeartbeatAt)
	}

	resp.Secret = service.Secret

	if s.bootstrapElementId != nil {
		resp.BootstrapElementId  = s.bootstrapElementId.String()
	}
	if s.bootstrapUserId != nil {
		resp.BootstrapUserId  = s.bootstrapUserId.String()
	}

	// Generate an event.
	oid := service.Id.String()
	eh := subscription.EventHeader{
		Type: eventType,
		Code: int32(identity_v1.EventCode_EC_SERVICE),
		SourceType: int32(event_v1.EventSourceType_EST_IDENTITY),
		SourceId: s.config.ServiceId,
		Id: uuid.New().String(),
		ObjectId: &oid,
	}
	if service.UpdatedAt != nil {
		eh.Time = service.UpdatedAt
	} else {
		eh.Time = &service.CreatedAt
	}
	e := subscription.Event{
		Header: eh,
		Object: &service,
	}
	go s.sm.Notify(&e)

	return resp, nil
}

func (s *Server) UnregisterService(ctx context.Context, req *identity_v1.UnregisterServiceRequest) (resp *identity_v1.UnregisterServiceResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid UnregisterServiceRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	log.Info().Msg(fmt.Sprintf("UnregisterService(%+v)", req))
	id := uuid.Nil
	if idParsed, err := uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid uuid Id")
	} else {
		id = idParsed
	}

	var service store.Service
	res := s.db.Where(&store.Service{Id: id}).First(&service)
	if res.RowsAffected < 1 {
		return nil, status.Errorf(codes.NotFound, "Service not found")
	}
	if service.Secret != req.Secret {
		return nil, status.Errorf(codes.Unauthenticated, "Invalid service secret")
	}

	s.db.Delete(&service)
	resp = &identity_v1.UnregisterServiceResponse{Header: &identity_v1.ResponseHeader{}}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	// Generate an event.
	oid := service.Id.String()
	eh := subscription.EventHeader{
		Type: int32(event_v1.EventType_ET_DELETED),
		Code: int32(identity_v1.EventCode_EC_SERVICE),
		SourceType: int32(event_v1.EventSourceType_EST_IDENTITY),
		SourceId: s.config.ServiceId,
		Id: uuid.New().String(),
		ObjectId: &oid,
	}
	if service.UpdatedAt != nil {
		eh.Time = service.UpdatedAt
	} else {
		eh.Time = &service.CreatedAt
	}
	e := subscription.Event{
		Header: eh,
		Object: &service,
	}
	go s.sm.Notify(&e)

	return resp, nil
}

func (s *Server) UpdateService(ctx context.Context, req *identity_v1.UpdateServiceRequest) (resp *identity_v1.UpdateServiceResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid UpdateServiceRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	log.Info().Msg(fmt.Sprintf("UpdateService(%+v)", req))
	id := uuid.Nil
	if idParsed, err := uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid uuid Id")
	} else {
		id = idParsed
	}

	var service store.Service
	res := s.db.Where(&store.Service{Id: id}).First(&service)
	if res.RowsAffected < 1 {
		return nil, status.Errorf(codes.NotFound, "Service not found")
	}
	if service.Secret != req.Secret {
		return nil, status.Errorf(codes.Unauthenticated, "Invalid service secret")
	}

	// If there is a change to the mutable fields, the UpdatedAt field is
	// set; otherwise, only the HeartbeatAt field is updated.
	eventType := int32(event_v1.EventType_ET_UPDATED)
	if req.Enabled != nil {
		if service.Enabled != *req.Enabled {
			if *req.Enabled {
				eventType = int32(event_v1.EventType_ET_ENABLED)
			} else {
				eventType = int32(event_v1.EventType_ET_DISABLED)
			}
		}
		service.Enabled = *req.Enabled
	} else {
		t := time.Now().UTC()
		service.HeartbeatAt = &t
	}
	s.db.Save(&service)

	resp = &identity_v1.UpdateServiceResponse{Header: &identity_v1.ResponseHeader{}}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	resp.Service = &identity_v1.Service{}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate
	service.ToProto(resp.Service, elaborate)

	// Generate an event.
	oid := service.Id.String()
	eh := subscription.EventHeader{
		Type: eventType,
		Code: int32(identity_v1.EventCode_EC_SERVICE),
		SourceType: int32(event_v1.EventSourceType_EST_IDENTITY),
		SourceId: s.config.ServiceId,
		Id: uuid.New().String(),
		ObjectId: &oid,
	}
	if service.UpdatedAt != nil {
		eh.Time = service.UpdatedAt
	} else {
		eh.Time = &service.CreatedAt
	}
	e := subscription.Event{
		Header: eh,
		Object: &service,
	}
	go s.sm.Notify(&e)

	return resp, nil
}

func (s *Server) GetUser(ctx context.Context, req *identity_v1.GetUserRequest) (resp *identity_v1.GetUserResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetUserRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	id := uuid.Nil
	if idParsed, err := uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid uuid Id")
	} else {
		id = idParsed
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetUser(%+v)", req))

	var user store.User
	q := s.db.Where(&store.User{Id: id})
	if elaborate {
		q = q.Preload("RoleBindings.Role").
			Preload("PrimaryEmailAddress").
			Preload("EmailAddresses")
	}
	res := q.First(&user)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "User not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	out := identity_v1.User{}
	user.ToProto(&out, elaborate)
	resp = &identity_v1.GetUserResponse{
		Header: &identity_v1.ResponseHeader{},
		User: &out,
	}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	return resp, nil
}

func (s *Server) GetElement(ctx context.Context, req *identity_v1.GetElementRequest) (resp *identity_v1.GetElementResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetElementRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	id := uuid.Nil
	if idParsed, err := uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "invalid uuid Id")
	} else {
		id = idParsed
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetElement(%+v)", req))

	var element store.Element
	q := s.db.Where(&store.Element{Id: id})
	if elaborate {
		q = q.Preload("Attributes")
	}
	res := q.First(&element)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Element not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	out := identity_v1.Element{}
	element.ToProto(&out, elaborate)
	resp = &identity_v1.GetElementResponse{
		Header: &identity_v1.ResponseHeader{},
		Element: &out,
	}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	return resp, nil
}

func (s *Server) GetToken(ctx context.Context, req *identity_v1.GetTokenRequest) (resp *identity_v1.GetTokenResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid TokenRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetToken(%+v)", req))

	var targetTok store.Token
	q := s.db.Where(&store.Token{Token: req.GetToken()})
	if elaborate {
		q = q.Preload("RoleBindings.RoleBinding.Role")
	}
	res := q.First(&targetTok)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Token not found")
	}

	if targetTok.ExpiresAt != nil && targetTok.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		return nil, status.Errorf(codes.Unauthenticated, "token expired")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	out := identity_v1.Token{}
	targetTok.ToProto(&out, elaborate)
	resp = &identity_v1.GetTokenResponse{
		Header: &identity_v1.ResponseHeader{},
		Token: &out,
	}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	return resp, nil
}

func (s *Server) ValidateToken(ctx context.Context, req *identity_v1.ValidateTokenRequest) (resp *identity_v1.ValidateTokenResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid TokenRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetToken(%+v)", req))

	var targetTok store.Token
	q := s.db.Where(&store.Token{Token: req.GetToken()})
	if elaborate {
		q = q.Preload("RoleBindings.RoleBinding.Role")
	}
	res := q.First(&targetTok)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Token not found")
	}

	if targetTok.ExpiresAt != nil && targetTok.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		return nil, status.Errorf(codes.Unauthenticated, "token expired")
	}

	resp = &identity_v1.ValidateTokenResponse{
		Header: &identity_v1.ResponseHeader{},
	}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	return resp, nil
}

func (s *Server) GetRoleList(ctx context.Context, req *identity_v1.RoleListRequest) (resp *identity_v1.RoleListResponse, err error) {
	var roles []store.Role = make([]store.Role, 0)
	s.db.Find(&roles)

	resp = &identity_v1.RoleListResponse{
		Header: &identity_v1.ResponseHeader{},
	}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	for _, role := range roles {
		rrole := identity_v1.Role{
			Id:          role.Id.String(),
			Name:        role.Name,
			Value:       int32(role.Value),
			Description: role.Description,
		}
		resp.Roles = append(resp.Roles, &rrole)
	}

	return resp, nil
}

func (s *Server) GetServiceList(ctx context.Context, req *identity_v1.ServiceListRequest) (resp *identity_v1.ServiceListResponse, err error) {
	var services []store.Service = make([]store.Service, 0)
	s.db.Find(&services)

	resp = &identity_v1.ServiceListResponse{
		Header: &identity_v1.ResponseHeader{},
	}
	if req.Header != nil && req.Header.ReqId != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	for _, service := range services {
		rservice := identity_v1.Service{
			Id:             service.Id.String(),
			Name:           service.Name,
			Kind:           service.Kind,
			Endpoint:       service.Endpoint,
			EndpointApiUri: service.EndpointApiUri,
			Description:    service.Description,
			Version:        service.Version,
			ApiVersion:     service.ApiVersion,
			Enabled:        service.Enabled,
			CreatedAt:      timestamppb.New(service.CreatedAt),
		}
		if service.UpdatedAt != nil {
			rservice.UpdatedAt = timestamppb.New(*service.UpdatedAt)
		}
		if service.HeartbeatAt != nil {
			rservice.HeartbeatAt = timestamppb.New(*service.HeartbeatAt)
		}
		resp.Services = append(resp.Services, &rservice)
	}

	return resp, nil
}

func EventToProto(e *subscription.Event, include bool, elaborate bool) (*identity_v1.Event, error) {
	ep := &identity_v1.Event{
		Header: e.Header.ToProto(),
	}

	if include && e.Object != nil {
		switch v := e.Object.(type) {
		case *store.User:
			x := new(identity_v1.User)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &identity_v1.Event_User{User: x}
		case *store.Element:
			x := new(identity_v1.Element)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &identity_v1.Event_Element{Element: x}
		case *store.Role:
			x := new(identity_v1.Role)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &identity_v1.Event_Role{Role: x}
		case *store.RoleBinding:
			x := new(identity_v1.RoleBinding)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &identity_v1.Event_RoleBinding{RoleBinding: x}
		case *store.Token:
			x := new(identity_v1.Token)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &identity_v1.Event_Token{Token: x}
		case *store.Service:
			x := new(identity_v1.Service)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &identity_v1.Event_Service{Service: x}
		default:
			return nil, fmt.Errorf("unknown event object type %T", v)
		}
	}

	log.Debug().Msg(fmt.Sprintf("EventToProto: %+v", ep))

	return ep, nil
}

func (s *Server) Subscribe(req *identity_v1.SubscribeRequest, stream identity_v1.Identity_SubscribeServer) error {
	if req == nil || req.Header == nil || req.Header.ReqId == nil {
		return status.Errorf(codes.InvalidArgument, "Invalid SubscribeRequest")
	}
	if err := req.Validate(); err != nil {
		return status.Errorf(codes.InvalidArgument, err.Error())
	}

	var filters []subscription.EventFilter
	if req.Filters != nil && len(req.Filters) > 0 {
		filters = make([]subscription.EventFilter, 0, len(req.Filters))
		for _, f := range req.Filters {
			filters = append(filters, subscription.EventFilterFromProto(f))
		}
	}
	ch := make(chan *subscription.Event)
	sid := *req.Header.ReqId
	endpoint := ""
	if p, ok := peer.FromContext(stream.Context()); ok {
		endpoint = p.Addr.String()
	}
	if _, err := s.sm.Subscribe(sid, filters, ch, endpoint, subscription.Rpc, nil); err != nil {
		return status.Errorf(codes.Internal, err.Error())
	}

	// Handle write panics as well as the regular return cases.
	defer func() {
		if (true || recover() != nil) {
			s.sm.Unsubscribe(sid)
		}
	}()

	include := req.Include == nil || *req.Include
	elaborate := req.Header.Elaborate != nil && *req.Header.Elaborate

	// Watch for events and client connection error conditions
	// (e.g. disconnect).
	for {
		select {
		case e := <- ch:
			if ep, err := EventToProto(e, include, elaborate); err != nil {
				log.Error().Err(err).Msg("failed to marshal event to RPC")
				continue
			} else {
				events := make([]*identity_v1.Event, 0, 1)
				events = append(events, ep)
				resp := &identity_v1.SubscribeResponse{
					Header: &identity_v1.ResponseHeader{
						ReqId: req.Header.ReqId,
					},
					Events: events,
				}
				if err := stream.Send(resp); err != nil {
					break
				}
			}
		case <- stream.Context().Done():
			log.Debug().Msg(fmt.Sprintf("stream closed unexpectedly (%s)", sid))
			return nil
		}
	}

	return nil
}
