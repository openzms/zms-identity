// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package southbound

import (
	"context"
	"errors"
	"net"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"
	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"

	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/config"
)

type Server struct {
	identity.UnimplementedIdentityServer
	db       *gorm.DB
	sm       *subscription.SubscriptionManager[*subscription.Event]
	config   *config.Config
	listener *net.Listener
	grpc     *grpc.Server
	running  bool
	bootstrapElementId *uuid.UUID
	bootstrapUserId    *uuid.UUID
}

func NewServer(serverConfig *config.Config, db *gorm.DB, sm *subscription.SubscriptionManager[*subscription.Event], bootstrapElementId *uuid.UUID, bootstrapUserId *uuid.UUID) (server *Server, err error) {
	err = nil
	server = &Server{config: serverConfig}
	/*
		if !serverConfig.Debug {
			gin.SetMode(gin.ReleaseMode)
		}
	*/
	server.db = db
	server.sm = sm
	if lis, err := net.Listen("tcp", serverConfig.RpcEndpointListen); err != nil {
		return nil, err
	} else {
		server.listener = &lis
	}
	server.bootstrapElementId = bootstrapElementId
	server.bootstrapUserId = bootstrapUserId
	server.grpc = grpc.NewServer()

	identity.RegisterIdentityServer(server.grpc, server)

	server.running = false

	return server, nil
}

func (s *Server) Run() (err error) {
	if s.running {
		return errors.New("server already running")
	}
	go func() {
		log.Debug().Msg("starting gRPC server")
		if err = s.grpc.Serve(*s.listener); err != nil {
			log.Error().Err(err).Msg("gRPC server error")
		}
	}()
	s.running = true
	return nil
}

func (s *Server) Shutdown(ctx context.Context) (err error) {
	if !s.running {
		return errors.New("server not running")
	}

	shutdownCtx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	s.grpc.GracefulStop()
	cancel()
	s.running = false

	select {
	case <-shutdownCtx.Done():
		if err = ctx.Err(); err != nil {
			log.Error().Err(err).Msg("failed to gracefully stop gRPC server")
		} else {
			log.Info().Msg("gracefully stopped gRPC server")
		}
		s.running = false
		return err
	}

	return nil
}
