// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package northbound

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"github.com/gorilla/websocket"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/token"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/policy"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"
	event_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
	identity_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"

	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/store"
	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/version"
)

func (s *Server) SetupHealthRoutes() error {
	s.ginHealth.GET("/health/alive", s.GetHealthAlive)
	s.ginHealth.GET("/health/ready", s.GetHealthReady)
	return nil
}

func (s *Server) GetHealthAlive(c *gin.Context) {
	c.Status(http.StatusOK)
}

func (s *Server) GetHealthReady(c *gin.Context) {
	if db, err := s.db.DB(); err == nil {
		if err := db.Ping(); err == nil {
			c.Status(http.StatusOK)
			return
		}
	}
	c.Status(http.StatusServiceUnavailable)
}

func CheckElaborateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Elaborate"); tokenString == "1" || tokenString == "true" || tokenString == "True" {
			c.Set("zms.elaborate", true)
		} else {
			c.Set("zms.elaborate", false)
		}
	}
}

func LoadToken(s *Server, t *string, elaborate bool, tok *store.Token) *gorm.DB {
	q := s.db.Where(&store.Token{Token: *t})
	if elaborate {
		q = q.Preload("RoleBindings.RoleBinding.Role")
	}
	res := q.First(tok)
	if res.RowsAffected > 0 {
		//
		// NB: sort the role bindings in descending order, so that policy
		// middleware will match the most encompassing scope first.
		//
		tok.SortRoleBindings()
	}

	return res
}

func LoadTokenById(s *Server, tId *uuid.UUID, elaborate bool, tok *store.Token) *gorm.DB {
	q := s.db.Where(&store.Token{Id: *tId})
	if elaborate {
		q = q.Preload("RoleBindings.RoleBinding.Role")
	}
	res := q.First(tok)
	if res.RowsAffected > 0 && tok.RoleBindings != nil && len(tok.RoleBindings) > 0 {
		//
		// NB: sort the role bindings in descending order, so that policy
		// middleware will match the most encompassing scope first.
		//
		tok.SortRoleBindings()
	}

	return res
}

func CheckTokenMiddleware(s *Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("X-Api-Token")
		if tokenString == "" {
			c.Set("zms.token", nil)
			return
		}
		if err := token.Validate(tokenString); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid token"})
			return
		}
		var tok store.Token
		res := LoadToken(s, &tokenString, true, &tok)
		if res.Error != nil || res.RowsAffected != 1 {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "nonexistent token"})
			return
		}
		if tok.ExpiresAt != nil && tok.ExpiresAt.Compare(time.Now().UTC()) < 1 {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "expired token"})
			return
		}

		c.Set("zms.token", &tok)
	}
}

func CorsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func (s *Server) SetupRoutes() error {
	v1 := s.gin.Group("/v1")

	v1.Use(CorsMiddleware())
	v1.Use(CheckElaborateMiddleware())
	v1.Use(CheckTokenMiddleware(s))

	v1.GET("/version", s.GetVersion)

	v1.GET("/tokens/this", s.GetThisToken)
	v1.POST("/tokens", s.CreateToken)
	v1.DELETE("/tokens", s.DeleteThisToken)
	v1.GET("/tokens/:token_id", s.GetToken)
	v1.DELETE("/tokens/:token_id", s.DeleteToken)

	v1.GET("/users", s.ListUsers)
	v1.POST("/users", s.CreateUser)
	v1.GET("/users/:user_id", s.GetUser)
	v1.PUT("/users/:user_id", s.UpdateUser)
	v1.DELETE("/users/:user_id", s.DeleteUser)
	v1.GET("/users/:user_id/rolebindings", s.ListUserRoleBindings)
	v1.GET("/users/:user_id/elements", s.ListUserElements)
	v1.GET("/users/:user_id/tokens", s.ListUserTokens)

	v1.GET("/rolebindings", s.ListRoleBindings)
	v1.POST("/rolebindings", s.CreateRoleBinding)
	//v1.GET("/rolebindings/:rolebinding_id", s.GetRoleBinding)
	v1.PUT("/rolebindings/:rolebinding_id", s.UpdateRoleBinding)
	v1.DELETE("/rolebindings/:rolebinding_id", s.DeleteRoleBinding)

	v1.GET("/elements", s.ListElements)
	v1.POST("/elements", s.CreateElement)
	v1.GET("/elements/:element_id", s.GetElement)
	v1.PUT("/elements/:element_id", s.UpdateElement)
	v1.DELETE("/elements/:element_id", s.DeleteElement)

	v1.GET("/services", s.ListServices)

	v1.GET("/roles", s.GetRoles)

	v1.GET("/subscriptions", s.ListSubscriptions)
	v1.POST("/subscriptions", s.CreateSubscription)
	v1.DELETE("/subscriptions/:subscription_id", s.DeleteSubscription)
	v1.GET("/subscriptions/:subscription_id/events", s.GetSubscriptionEvents)

	return nil
}

func (s *Server) CheckTokenValue(tokenString string, tok *store.Token) (int, error) {
	if err := token.Validate(tokenString); err != nil {
		return http.StatusBadRequest, fmt.Errorf("invalid token")
	}
	res := LoadToken(s, &tokenString, true, tok)
	if res.Error != nil || res.RowsAffected != 1 {
		return http.StatusForbidden, fmt.Errorf("nonexistent token")
	}
	if tok.ExpiresAt != nil && tok.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		return http.StatusForbidden, fmt.Errorf("expired token")
	}

	return http.StatusOK, nil
}

func (s *Server) CheckPolicyMiddleware(t *store.Token, targetUserId *uuid.UUID, targetElementId *uuid.UUID, policies []policy.Policy) (bool, string) {
	for _, p := range policies {
		log.Debug().Msg(fmt.Sprintf("policy: %s", p.Name))
		for _, trb := range t.RoleBindings {
			// NB: skip unapproved or deleted RoleBindings.
			//
			// NB: endpoints that require a User to access even if no valid
			// RoleBindings exist must include a MatchUser policy.
			if trb.RoleBinding.ApprovedAt == nil || trb.RoleBinding.DeletedAt != nil {
				continue
			}

			if match := p.Check(trb.RoleBinding.Role.Value, &t.UserId, trb.RoleBinding.ElementId, targetUserId, targetElementId); match == true {
				log.Debug().Msg(fmt.Sprintf("policy match: %s", p.Name))
				return true, p.Name
			} else {
				log.Debug().Msg(fmt.Sprintf("policy mismatch: %s", p.Name))
			}
		}
	}
	return false, ""
}

func (s *Server) CheckPolicyMiddlewareExt(t *store.Token, targetUserId *uuid.UUID, targetElementIds []*uuid.UUID, policies []policy.Policy) (bool, string, *store.RoleBinding) {
	for _, p := range policies {
		log.Debug().Msg(fmt.Sprintf("policy: %s", p.Name))
		for _, trb := range t.RoleBindings {
			// NB: skip unapproved or deleted RoleBindings.
			//
			// NB: endpoints that require a User to access even if no valid
			// RoleBindings exist must include a MatchUser policy.
			if trb.RoleBinding.ApprovedAt == nil || trb.RoleBinding.DeletedAt != nil {
				continue
			}

			if len(targetElementIds) > 0 {
				for _, targetElementId := range targetElementIds {
					if match := p.Check(trb.RoleBinding.Role.Value, &t.UserId, trb.RoleBinding.ElementId, targetUserId, targetElementId); match == true {
						log.Debug().Msg(fmt.Sprintf("policy match: %s", p.Name))
						return true, p.Name, trb.RoleBinding
					} else {
						log.Debug().Msg(fmt.Sprintf("policy mismatch: %s", p.Name))
					}
				}
			} else {
				if match := p.Check(trb.RoleBinding.Role.Value, &t.UserId, trb.RoleBinding.ElementId, targetUserId, nil); match == true {
					log.Debug().Msg(fmt.Sprintf("policy match: %s", p.Name))
					return true, p.Name, trb.RoleBinding
				} else {
					log.Debug().Msg(fmt.Sprintf("policy mismatch: %s", p.Name))
				}
			}
		}
	}
	return false, "", nil
}

func GetPaginateParams(c *gin.Context) (int, int) {
	page, _ := strconv.Atoi(c.Query("page"))
	if page < 1 {
		page = 1
	}
	itemsPerPage, _ := strconv.Atoi(c.Query("items_per_page"))
	if itemsPerPage < 10 {
		itemsPerPage = 10
	} else if itemsPerPage > 100 {
		itemsPerPage = 100
	}
	return page, itemsPerPage
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func (s *Server) GetVersion(c *gin.Context) {
	c.JSON(http.StatusOK, version.GetVersion())
}

type CreateTokenModel struct {
	Method         string          `json:"method" binding:"required,oneof=password token idp"`
	Credential     *json.RawMessage `json:"credential"`
	RoleBindingIds []uuid.UUID     `json:"role_binding_ids"`
	ExpiresAt      *time.Time      `json:"expires_at"`
	AdminIfBound   *bool           `json:"admin_if_bound"`
	TokenType      *string         `json:"token_type" binding:"omitempty,oneof=pat user"`
}

type CredentialPasswordModel struct {
	Username string `json:"username" binding:"required"`
	Password []byte `json:"password" binding:"required"`
}

type CredentialTokenModel struct {
	Token string `json:"token" binding:"required"`
}

type CredentialIdpModel struct {
	Idp         string `json:"idp" binding:"required,oneof=github google cilogon"`
	AccessToken string `json:"access_token" binding:"required"`
}

type GithubUserResponse struct {
	Id    int    `json:"id" binding:"required"`
	Email string `json:"email" binding:"required"`
	Url   string `json:"url" binding:"required"`
	Name  string `json:"name"`
	Login string `json:"login"`
}

func ValidateGithubToken(cim CredentialIdpModel) (uii *store.UserIdpIdentity, err error) {
	client := &http.Client{}
	githubUserApi := "https://api.github.com/user"
	request, err := http.NewRequest("GET", githubUserApi, nil)
	request.Header.Add("Accept", "application/vnd.github.v3+json")
	request.Header.Add("Authorization", "Bearer "+cim.AccessToken)
	if response, err := client.Do(request); err != nil {
		return nil, err
	} else if response.StatusCode != http.StatusOK {
		if response.StatusCode == http.StatusUnauthorized || response.StatusCode == http.StatusForbidden {
			return nil, fmt.Errorf("unauthorized github token (%d)", response.StatusCode)
		} else {
			return nil, fmt.Errorf("unexpected github error (%d)", response.StatusCode)
		}
	} else {
		var gur GithubUserResponse
		body, _ := io.ReadAll(response.Body)
		response.Body.Close()
		if err := json.Unmarshal(body, &gur); err != nil {
			return nil, fmt.Errorf("unexpected response from github user api (%s)", err.Error())
		}
		if err := binding.Validator.ValidateStruct(&gur); err != nil {
			return nil, fmt.Errorf("invalid response from github user api (%s)", err.Error())
		}

		uii := &store.UserIdpIdentity{
			Sub:          fmt.Sprintf("%s+%d", gur.Url, gur.Id),
			Iss:          githubUserApi,
			Idp:          "github",
			Name:         gur.Name,
			EmailAddress: gur.Email,
		}
		return uii, nil
	}
}

type GoogleUserResponse struct {
	Sub           string `json:"sub" binding:"required"`
	Email         string `json:"email" binding:"required"`
	EmailVerified bool   `json:"email_verified" binding:"required"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
}

func ValidateGoogleToken(cim CredentialIdpModel) (uiid *store.UserIdpIdentity, err error) {
	client := &http.Client{}
	googleUserApi := "https://openidconnect.googleapis.com/v1/userinfo"
	request, err := http.NewRequest("GET", googleUserApi, nil)
	request.Header.Add("Authorization", "Bearer " + cim.AccessToken)
	if response, err := client.Do(request); err != nil {
		return nil, err
	} else if response.StatusCode != http.StatusOK {
		if response.StatusCode == http.StatusUnauthorized || response.StatusCode == http.StatusForbidden {
			return nil, fmt.Errorf("unauthorized google token (%d)", response.StatusCode)
		} else {
			return nil, fmt.Errorf("unexpected google error (%d)", response.StatusCode)
		}
	} else {
		var gur GoogleUserResponse
		body, _ := io.ReadAll(response.Body)
		log.Debug().Msg(fmt.Sprintf("googleUserResponse: %+v", string(body)))
		response.Body.Close()
		if err := json.Unmarshal(body, &gur); err != nil {
			return nil, fmt.Errorf("unexpected response from google user api (%s)", err.Error())
		}
		if err := binding.Validator.ValidateStruct(&gur); err != nil {
			return nil, fmt.Errorf("invalid response from google user api (%s)", err.Error())
		}
		if !gur.EmailVerified {
			return nil, fmt.Errorf("google user email not verified")
		}

		uii := &store.UserIdpIdentity{
			Sub:          gur.Sub,
			Iss:          googleUserApi,
			Idp:          "google",
			Name:         gur.Name,
			EmailAddress: gur.Email,
		}
		return uii, nil
	}
}

type CilogonUserResponse struct {
	Sub           string `json:"sub" binding:"required"`
	Email         string `json:"email" binding:"required"`
	Name          string `json:"name"`
}

func ValidateCilogonToken(cim CredentialIdpModel) (uiid *store.UserIdpIdentity, err error) {
	client := &http.Client{}
	cilogonUserApi := "https://cilogon.org/oauth2/userinfo"
	request, err := http.NewRequest("GET", cilogonUserApi, nil)
	request.Header.Add("Authorization", "Bearer " + cim.AccessToken)
	if response, err := client.Do(request); err != nil {
		return nil, err
	} else if response.StatusCode != http.StatusOK {
		if response.StatusCode == http.StatusUnauthorized || response.StatusCode == http.StatusForbidden {
			return nil, fmt.Errorf("unauthorized cilogon token (%d)", response.StatusCode)
		} else {
			return nil, fmt.Errorf("unexpected cilogon error (%d)", response.StatusCode)
		}
	} else {
		var cur CilogonUserResponse
		body, _ := io.ReadAll(response.Body)
		log.Debug().Msg(fmt.Sprintf("cilogonUserResponse: %+v", string(body)))
		response.Body.Close()
		if err := json.Unmarshal(body, &cur); err != nil {
			return nil, fmt.Errorf("unexpected response from cilogon user api (%s)", err.Error())
		}
		if err := binding.Validator.ValidateStruct(&cur); err != nil {
			return nil, fmt.Errorf("invalid response from cilogon user api (%s)", err.Error())
		}

		uii := &store.UserIdpIdentity{
			Sub:          cur.Sub,
			Iss:          cilogonUserApi,
			Idp:          "cilogon",
			EmailAddress: cur.Email,
		}
		if cur.Name != "" {
			uii.Name = cur.Name
		}
		return uii, nil
	}
}

func ValidateIdpToken(cim CredentialIdpModel) (uiid *store.UserIdpIdentity, err error) {
	if cim.Idp == "github" {
		uiid, err = ValidateGithubToken(cim)
	} else if cim.Idp == "google" {
		uiid, err = ValidateGoogleToken(cim)
	} else if cim.Idp == "cilogon" {
		uiid, err = ValidateCilogonToken(cim)
	}
	return uiid, err
}

func (s *Server) CreateAdminTokenForUser(u *store.User, expiresAt *time.Time) (*store.Token, error) {
	var adminRole store.Role
	res := s.db.Where(&store.Role{Name: "admin"}).First(&adminRole)
	if res.RowsAffected != 1 || res.Error != nil {
		return nil, fmt.Errorf("no admin role to bind")
	}
	var roleBinding store.RoleBinding
	res = s.db.Where(&store.RoleBinding{UserId: u.Id, ElementId: nil, RoleId: adminRole.Id, DeletedAt: nil}).Where("approved_at is not null").First(roleBinding)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, fmt.Errorf("unauthorized to role")
	}
	tokenValue, err := token.Generate(token.UserToken)
	if err != nil {
		return nil, fmt.Errorf("failed to generate token: %s", err.Error())
	}
	if expiresAt == nil {
		t := time.Now().UTC().Add(s.config.UserTokenDuration)
		expiresAt = &t
	}
	tok := store.Token{
		Id:        uuid.New(),
		UserId:    u.Id,
		Token:     tokenValue,
		ExpiresAt: expiresAt,
	}
	tokenRoleBinding := store.TokenRoleBinding{
		TokenId:       tok.Id,
		RoleBindingId: roleBinding.Id,
	}
	s.db.Create(&tok)
	s.db.Create(&tokenRoleBinding)
	return &tok, nil
}

func (s *Server) CreateDefaultTokenForUser(u *store.User, expiresAt *time.Time, adminIfBound *bool) (*store.Token, error) {
	roleBindings := make([]store.RoleBinding, 0, 4)
	res := s.db.Where(&store.RoleBinding{UserId: u.Id}).
		Where("deleted_at is NULL").
		Where("approved_at is not null").
		Preload("Role").
		Find(&roleBindings)
	if res.Error != nil {
		return nil, fmt.Errorf("unexpected database error selecting roles")
	}
	tokenValue, err := token.Generate(token.UserToken)
	if err != nil {
		return nil, fmt.Errorf("failed to generate token: %s", err.Error())
	}
	if expiresAt == nil {
		t := time.Now().UTC().Add(s.config.UserTokenDuration)
		expiresAt = &t
	}
	tok := store.Token{
		Id:        uuid.New(),
		UserId:    u.Id,
		Token:     tokenValue,
		ExpiresAt: expiresAt,
	}
	tokenRoleBindings := make([]store.TokenRoleBinding, 0, len(roleBindings))
	for _, rb := range roleBindings {
		// Skip admin role if caller doesn't explicitly want admin privs.
		if rb.Role.Name == "admin" && (adminIfBound == nil || !*adminIfBound) {
			continue
		}
		tokenRoleBindings = append(tokenRoleBindings, store.TokenRoleBinding{RoleBindingId: rb.Id})
	}
	tok.RoleBindings = tokenRoleBindings

	s.db.Create(&tok)

	// Refresh token
	res = LoadToken(s, &tok.Token, true, &tok)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, fmt.Errorf("failed to load token post-create")
	}

	return &tok, nil
}

func (s *Server) GetAutoAccountApprovalRole() (*store.Role, error) {
	var autoAccountApprovalRole store.Role
	if s.config.AutoAccountApproval == "viewer" || s.config.AutoAccountApproval == "account" {
		res := s.db.Where(&store.Role{Name: s.config.AutoAccountApproval}).First(&autoAccountApprovalRole)
		if res.Error != nil || res.RowsAffected != 1 {
			return nil, fmt.Errorf(
				"could not find %s role: %s",
				s.config.AutoAccountApproval, res.Error.Error())
		}
		return &autoAccountApprovalRole, nil
	} else {
		return nil, nil
	}
}

func (s *Server) CreateUserFromIdp(uii *store.UserIdpIdentity) (*store.User, error) {

	var autoAccountApprovalRole *store.Role
	if r, err := s.GetAutoAccountApprovalRole(); err != nil {
		log.Error().Err(err).Msg(fmt.Sprintf("failed to load AutoAccountApproval role: %s", err.Error()))
	} else {
		autoAccountApprovalRole = r
	}

	if res := s.db.Where("name = ? and deleted_at is NULL", uii.EmailAddress).First(&store.User{}); res.Error != nil {
		return nil, res.Error
	} else if res.RowsAffected > 0 {
		return nil, errors.New("user exists")
	}

	t := time.Now().UTC()
	user := store.User{
		Id:        uuid.New(),
		Name:      uii.EmailAddress,
		FullName:  &uii.Name,
		Enabled:   true,
		CreatedAt: t,
	}
	s.db.Create(&user)
	if uii.EmailAddress != "" {
		pea := store.UserEmailAddress{
			Id: uuid.New(),	UserId: user.Id,
			EmailAddress: uii.EmailAddress,
		}
		s.db.Create(&pea)
		user.PrimaryEmailAddressId = &pea.Id
		s.db.Save(&user)
	}
	uii.Id = uuid.New()
	uii.UserId = user.Id
	s.db.Create(&uii)

	if autoAccountApprovalRole != nil {
		roleBinding := store.RoleBinding{
			Id:        uuid.New(),
			RoleId:    autoAccountApprovalRole.Id,
			UserId:    user.Id,
			ElementId: nil,
			ApprovedAt: &t,
		}
		s.db.Create(&roleBinding)
	}

	return &user, nil
}

func (s *Server) CreateTokenFromCredentials(username string, password string, expiresAt *time.Time, adminIfBound *bool) (*store.Token, error) {
	var user store.User
	res := s.db.Where("name = ? and deleted_at is NULL", username).First(&user)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, errors.New("invalid credentials")
	}
	if err := bcrypt.CompareHashAndPassword(user.Password, []byte(password)); err != nil {
		return nil, errors.New("invalid credentials (m)")
	}
	if token, err := s.CreateDefaultTokenForUser(&user, expiresAt, adminIfBound); err != nil {
		return nil, err
	} else {
		return token, nil
	}
}

func (s *Server) CreateTokenFromIdp(uii *store.UserIdpIdentity, expiresAt *time.Time, adminIfBound *bool) (*store.Token, error) {
	var uuii store.UserIdpIdentity
	res := s.db.Where(&store.UserIdpIdentity{Sub: uii.Sub, Idp: uii.Idp}).Limit(1).Find(&uuii)
	if res.Error != nil {
		return nil, res.Error
	}
	var user store.User
	if res.RowsAffected < 1 {
		uuii = *uii
		uuii.Id = uuid.New()
		if userPtr, err := s.CreateUserFromIdp(&uuii); err != nil {
			return nil, err
		} else {
			// Refresh the user object.
			s.db.Where(&store.User{Id: userPtr.Id}).
				Preload("PrimaryUserEmailAddress").
				Preload("EmailAddresses").
				Preload("IdpIdentities").
				Preload("RoleBindings").
				First(&user)
		}
	} else {
		res2 := s.db.Where(&store.User{Id: uuii.UserId}).First(&user)
		if res2.RowsAffected < 1 {
			return nil, fmt.Errorf("unexpected user database inconsistency")
		}
	}
	if token, err := s.CreateDefaultTokenForUser(&user, expiresAt, adminIfBound); err != nil {
		return nil, err
	} else {
		return token, nil
	}
}

func (s *Server) CreateTokenFromToken(t *store.Token, tokenType token.TokenType, expiresAt *time.Time, roleBindingIds []uuid.UUID, adminIfBound *bool) (*store.Token, error) {

	newTokenId := uuid.New()
	var tokenRoleBindings []store.TokenRoleBinding

	if len(roleBindingIds) == 0 {
		// Generate a token with same bindings as Token t, except maybe not
		// admin).
		tokenRoleBindings = make([]store.TokenRoleBinding, 0, len(t.RoleBindings))

		// Ensure that roleBindingIds are a subset.
		for _, trb := range t.RoleBindings {
			// Skip admin role if caller doesn't explicitly want admin privs.
			if trb.RoleBinding.Role.Name == "admin" && (adminIfBound == nil || !*adminIfBound) {
				continue
			}
			ntrb := store.TokenRoleBinding{
				TokenId: newTokenId,
				RoleBindingId: trb.RoleBindingId,
			}
			tokenRoleBindings = append(tokenRoleBindings, ntrb)
		}
	} else {
		tokenRoleBindings = make([]store.TokenRoleBinding, 0, len(roleBindingIds))

		// Ensure that role bindings are a subset.
		haveAdmin := false
		for _, rbId := range roleBindingIds {
			var rb *store.RoleBinding
			for _, trb := range t.RoleBindings {
				if rbId == trb.RoleBindingId {
					rb = trb.RoleBinding
					break
				}
			}
			if rb == nil {
				return nil, fmt.Errorf("unauthorized role binding id %+v", rbId)
			}
			if rb.Role.Name == "admin" {
				haveAdmin = true
			}
			ntrb := store.TokenRoleBinding{
				TokenId: newTokenId,
				RoleBindingId: rb.Id,
			}
			tokenRoleBindings = append(tokenRoleBindings, ntrb)
		}
		if !haveAdmin && adminIfBound != nil && *adminIfBound {
			for _, trb := range t.RoleBindings {
				if trb.RoleBinding.Role.Name == "admin" {
					ntrb := store.TokenRoleBinding{
						TokenId: newTokenId,
						RoleBindingId: trb.RoleBindingId,
					}
					tokenRoleBindings = append(tokenRoleBindings, ntrb)
				}
			}
		}
	}

	td := s.config.PersonalAccessTokenDuration
	if tokenType != token.PersonalAccessToken {
		td = s.config.UserTokenDuration
	}
	newTokenString, err := token.Generate(tokenType)
	if err != nil {
		return nil, fmt.Errorf("failed to generate token: %s", err.Error())
	}
	if expiresAt == nil {
		ut := time.Now().UTC().Add(td)
		expiresAt = &ut
	}
	newToken := store.Token{
		Id:           newTokenId,
		UserId:       t.UserId,
		Token:        newTokenString,
		ExpiresAt:    expiresAt,
		RoleBindings: tokenRoleBindings,
	}
	s.db.Create(&newToken)

	// Refresh token
	res := LoadToken(s, &newTokenString, true, &newToken)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, fmt.Errorf("failed to load token post-create")
	}

	return &newToken, nil
}

func (s *Server) CreateToken(c *gin.Context) {
	var ctm CreateTokenModel
	if err := c.ShouldBindJSON(&ctm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	t := time.Now().UTC()
	if ctm.ExpiresAt != nil && ctm.ExpiresAt.Compare(t) < 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": fmt.Errorf("expires_at time must be greater than UTC now (delta %+v)", t.Sub(*ctm.ExpiresAt))})
		return
	}
	if ctm.TokenType != nil && *ctm.TokenType != "user" && *ctm.TokenType != "pat" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "token_type must be 'pat' or 'user'"})
		return
	}

	switch ctm.Method {
	case "password":
		var cpm CredentialPasswordModel
		if ctm.Credential != nil {
			if err := json.Unmarshal(*ctm.Credential, &cpm); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			if err := binding.Validator.ValidateStruct(&cpm); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
		} else if authString := c.GetHeader("Authorization"); authString == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must supply Basic Authorization header for password method with no credential"})
			return
		} else if matches := s.basicAuthRegexp.FindAllStringSubmatch(authString, -1); len(matches) != 1 {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must supply valid Basic Authorization header for password method with no credential"})
			return
		} else {
			authEncoded := matches[0][1]
			authDecoded := make([]byte, base64.StdEncoding.DecodedLen(len(authEncoded)))
			// Decode the basic auth credential.
			if n, err := base64.StdEncoding.Decode(authDecoded, []byte(authEncoded)); err != nil {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid base64 Basic Authorization username:password"})
				return
			} else {
				authDecoded = authDecoded[:n]
			}
			// Extract the username, password
			authBasic := strings.SplitN(string(authDecoded), ":", 2)
			if len(authBasic) != 2 || authBasic[0] == "" || authBasic[1] == "" {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "malformed Basic Authorization username:password"})
				return
			}
			// Ok, we have a reasonable username:password
			cpm.Username = authBasic[0]
			cpm.Password = []byte(authBasic[1])
		}
		if tok, err := s.CreateTokenFromCredentials(cpm.Username, string(cpm.Password), ctm.ExpiresAt, ctm.AdminIfBound); err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
			return
		} else {
			c.JSON(http.StatusCreated, tok)
			return
		}
	case "idp":
		var cim CredentialIdpModel
		if ctm.Credential == nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "credential must be set for idp method"})
			return
		}
		if err := json.Unmarshal(*ctm.Credential, &cim); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		if err := binding.Validator.ValidateStruct(&cim); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		uii, err := ValidateIdpToken(cim)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": fmt.Sprintf("invalid IdP token: %s", err.Error())})
			return
		}
		if tok, err := s.CreateTokenFromIdp(uii, ctm.ExpiresAt, ctm.AdminIfBound); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		} else {
			c.JSON(http.StatusCreated, tok)
			return
		}
	case "token":
		// Either we are going to create the new token based on the
		// X-Api-Token; or we are going to create it based on the token in
		// the credential; the token in the credential gets priority if set.
		var srcTok *store.Token

		// Unlike the prior two cases, this path requires an X-Api-Token; we
		// check it post-validation.
		var apiTok *store.Token
		var err error
		if apiTok, err = s.RequireContextToken(c); err != nil {
			return
		} else {
			srcTok = apiTok
		}

		// Validate payload, if any.  If not provided, the source token is
		// the X-Api-Token.
		var cm *CredentialTokenModel
		var cmTok *store.Token
		if ctm.Credential != nil {
			cm = &CredentialTokenModel{}
			if err := json.Unmarshal(*ctm.Credential, cm); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			if err := binding.Validator.ValidateStruct(cm); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}

			// Load token in payload, from which to derive new token.  NB: may
			// be different than X-Api-Token!
			cmTok = &store.Token{}
			if code, err := s.CheckTokenValue(cm.Token, cmTok); err != nil {
				c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
				return
			} else {
				srcTok = cmTok
			}
		}

		// Check that X-Api-Token is either owner of args token, or admin.
		// NB: if user, must have at least a RoleAccount rolebinding.
		if apiTok != nil && cmTok != nil {
			policies := policy.MakeMatchAdminOrUserRolePolicy(policy.RoleAccount, policy.GreaterOrEqual)
			if match, _ := s.CheckPolicyMiddleware(apiTok, &cmTok.UserId, nil, policies); !match {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
				return
			}
		}

		tokenType := token.PersonalAccessToken
		if ctm.TokenType != nil && *ctm.TokenType == "user" {
			tokenType = token.UserToken
		}

		if newToken, err := s.CreateTokenFromToken(srcTok, tokenType, ctm.ExpiresAt, ctm.RoleBindingIds, ctm.AdminIfBound); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		} else {
			c.JSON(http.StatusCreated, newToken)
			return
		}
	}

	c.AbortWithStatus(http.StatusBadRequest)
	return
}

func (s *Server) RequireContextToken(c *gin.Context) (*store.Token, error) {
	value, exists := c.Get("zms.token")
	if !exists || value == nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "missing X-Api-Token"})
		return nil, errors.New("missing X-Api-Token")
	}
	return value.(*store.Token), nil
}

func (s *Server) DeleteThisToken(c *gin.Context) {
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}
	s.db.Delete(&tok)

	// Generate an event.
	userIdStr := tok.UserId.String()
	oid := tok.Id.String()
	eh := subscription.EventHeader{
		Type: int32(event_v1.EventType_ET_DELETED),
		Code: int32(identity_v1.EventCode_EC_TOKEN),
		SourceType: int32(event_v1.EventSourceType_EST_IDENTITY),
		SourceId: s.serviceId,
		Id: uuid.New().String(),
		ObjectId: &oid,
		UserId: &userIdStr,
	}
	e := subscription.Event{
		Header: eh,
	}
	go s.sm.Notify(&e)

	c.Status(http.StatusOK)
}

func (s *Server) DeleteToken(c *gin.Context) {
	var tokenId uuid.UUID
	if tokenIdParsed, err := uuid.Parse(c.Param("token_id")); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing token_id path parameter"})
		return
	} else {
		tokenId = tokenIdParsed
	}

	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var targetTok store.Token
	res := s.db.Where(&store.Token{Id: tokenId}).First(&targetTok)
	if res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}
	// NB: this is the only exceptional call where user can take action with
	// no valid role (other than CreateToken for user/pass login, and
	// GetUser).
	policies := policy.MakeMatchAdminOrUserRolePolicy(policy.RoleAccount, policy.GreaterOrEqual)
	if match, _ := s.CheckPolicyMiddleware(tok, &targetTok.UserId, nil, policies); !match && targetTok.Token != tok.Token {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if targetTok.ExpiresAt != nil && targetTok.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "token expired"})
		return
	}

	s.db.Delete(&targetTok)

	// Generate an event.
	userIdStr := targetTok.UserId.String()
	oid := targetTok.Id.String()
	eh := subscription.EventHeader{
		Type: int32(event_v1.EventType_ET_DELETED),
		Code: int32(identity_v1.EventCode_EC_TOKEN),
		SourceType: int32(event_v1.EventSourceType_EST_IDENTITY),
		SourceId: s.serviceId,
		Id: uuid.New().String(),
		ObjectId: &oid,
		UserId: &userIdStr,
	}
	e := subscription.Event{
		Header: eh,
	}
	go s.sm.Notify(&e)

	c.Status(http.StatusOK)
}

func (s *Server) GetThisToken(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check target token expiry; perhaps not strictly necessary.
	if tok.ExpiresAt != nil && tok.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "token expired"})
		return
	}

	c.JSON(http.StatusOK, tok)
}

func (s *Server) GetToken(c *gin.Context) {
	// Check required path parameters.
	var tokenId uuid.UUID
	if tokenIdParsed, err := uuid.Parse(c.Param("token_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		tokenId = tokenIdParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	elaborate := c.GetBool("zms.elaborate")

	// Optimize: if token_id == tok.id and caller does not request
	// elaborate, simply return.
	if tokenId == tok.Id && !elaborate{
		c.JSON(http.StatusOK, tok)
		return
	}

	// Load target token, to check if not == caller token.
	var targetTok store.Token
	res := LoadTokenById(s, &tokenId, elaborate, &targetTok)
	if res.Error != nil || res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrUserRolePolicy(policy.RoleAccount, policy.GreaterOrEqual)
	if match, _ := s.CheckPolicyMiddleware(tok, &targetTok.UserId, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Check target token expiry; perhaps not strictly necessary.
	if targetTok.ExpiresAt != nil && targetTok.ExpiresAt.Compare(time.Now().UTC()) < 1 {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "token expired"})
		return
	}

	c.JSON(http.StatusOK, targetTok)
}

type ListUsersQueryParams struct {
	// NB: validator validates this, but gin-gonic does not have support for
	// using uuid.UUID as a type via TextUnmarshaler
	// (https://github.com/gin-gonic/gin/pull/3045), so we have to
	// workaround and do the extra unmarshal.
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	//RoleId *string `form:"role_id" binding:"omitempty,uuid"`
	Enabled *bool   `form:"enabled" binding:"omitempty"`
	Deleted *bool   `form:"deleted" binding:"omitempty"`
	User    *string `form:"user" binding:"omitempty"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=name full_name created_at updated_at deleted_at enabled"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListUsers(c *gin.Context) {
	// Check optional filter query parameters.
	params := ListUsersQueryParams{}
	if err := c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check optional filter query parameters.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		} else {
			elementId = &idParsed
		}
	}
	var roleId *uuid.UUID
	if p, exists := c.GetQuery("role_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		} else {
			roleId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if policyName != policy.MatchAdmin &&
		(elementId == nil ||
		(params.Deleted != nil && *params.Deleted)) {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (must provide authorized filter when non-admin)"})
		return
	}

	var users []store.User
	q := s.db
	if elementId != nil {
		// NB: have to use manual join to support Count below.
		q = q.Joins("left join role_bindings on users.id=role_bindings.user_id").
			Where("role_bindings.element_id = ?", *elementId).
			Where("role_bindings.deleted_at is NULL")
		// gorm clearly cannot apply struct queries to joined tables.
		//&store.RoleBinding{ElementId: elementId, DeletedAt: nil})
	}
	if roleId != nil {
		q = q.Joins("left join role_bindings on users.id=role_bindings.user_id").
			Where("role_bindings.role_id = ?", *roleId)
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Users.deleted_at is NULL")
	} else {
		q = q.Where("Users.deleted_at is not NULL")
	}
	if params.Enabled != nil {
		if *params.Enabled {
			q = q.Where("Users.enabled is true")
		} else {
			q = q.Where("Users.enabled is false")
		}
	}
	if params.User != nil {
		q = q.Where(s.db.Where(store.MakeILikeClause(s.config, "name"), fmt.Sprintf("%%%s%%", *params.User)).Or(store.MakeILikeClause(s.config, "full_name"), fmt.Sprintf("%%%s%%", *params.User)))
	}
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RoleBindings.Role").Preload("PrimaryEmailAddress").Preload("EmailAddresses").Preload("IdpIdentities")
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order(*params.Sort + " " + sortDir)
	} else {
		q = q.Order("updated_at " + sortDir)
	}

	var total int64
	q.Model(&store.User{}).Count(&total)
	q.Scopes(store.Paginate(page, itemsPerPage)).Find(&users)

	c.JSON(http.StatusOK, gin.H{"users": users, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

type CreateUserModel struct {
	Name                    string   `json:"name" binding:"required,max=128"`
	GivenName               *string  `json:"given_name" binding:"omitempty,max=128"`
	FamilyName              *string  `json:"family_name" binding:"omitempty,max=128"`
	FullName                *string  `json:"full_name" binding:"omitempty,max=255"`
	Password                []byte   `json:"password" binding:"required"`
	PrimaryEmailAddress     string   `json:"primary_email_address" binding:"required,max=320"`
	SecondaryEmailAddresses []string `json:"secondary_email_addresses"`
}

func (s *Server) CreateUser(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var cm CreateUserModel
	if err := c.ShouldBindJSON(&cm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Additional input validation.
	if len(cm.Password) < 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "must supply valid password"})
		return
	}
	passwordHash, hperr := bcrypt.GenerateFromPassword(cm.Password, bcrypt.DefaultCost)
	if hperr != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid pasword"})
		return
	}

	// Check policy.  Admins or >=Operator users are allowed to create new
	// User accounts.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	if match, _, _ := s.CheckPolicyMiddlewareExt(tok, nil, tok.GetElementIds(), policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Check if user exists.
	if res := s.db.Where("name = ? and deleted_at is NULL", cm.Name).First(&store.User{}); res.RowsAffected == 1 {
		c.AbortWithStatusJSON(http.StatusConflict, gin.H{"error":fmt.Sprintf("user name %s unavailable", cm.Name)})
		return
	}

	var autoAccountApprovalRole *store.Role
	autoEnabled := false
	if r, err := s.GetAutoAccountApprovalRole(); err != nil {
		log.Error().Err(err).Msg(fmt.Sprintf("failed to load AutoAccountApproval role: %s", err.Error()))
	} else {
		autoAccountApprovalRole = r
		autoEnabled = true
	}

	uid := uuid.New()
	user := store.User{
		Id: uid, Name: cm.Name, Password: passwordHash,
		FamilyName: cm.FamilyName, FullName: cm.FullName, GivenName: cm.GivenName,
		Enabled: autoEnabled,
	}
	if res := s.db.Create(&user); res.Error != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	pea := store.UserEmailAddress{
		Id: uuid.New(), UserId: uid, EmailAddress: cm.PrimaryEmailAddress,
	}
	if res := s.db.Create(&pea); res.Error != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	user.PrimaryEmailAddressId = &pea.Id
	if res := s.db.Save(&user); res.Error != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	for _, sea := range cm.SecondaryEmailAddresses {
		se := store.UserEmailAddress{
			Id: uuid.New(), UserId: uid, EmailAddress: sea,
		}
		if res := s.db.Create(&se); res.Error != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
	}
	if autoAccountApprovalRole != nil {
		t := time.Now().UTC()
		roleBinding := store.RoleBinding{
			Id:        uuid.New(),
			RoleId:    autoAccountApprovalRole.Id,
			UserId:    user.Id,
			ElementId: nil,
			ApprovedAt: &t,
		}
		if res := s.db.Create(&roleBinding); res.Error != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
	}

	// Reload user
	q := s.db.Where(&store.User{Id: uid})
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RoleBindings.Role").Preload("PrimaryEmailAddress").Preload("EmailAddresses").Preload("IdpIdentities")
	}
	if res := q.First(&user); res.Error != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	// Generate an event.
	userIdStr := user.Id.String()
	eh := subscription.EventHeader{
		Type: int32(event_v1.EventType_ET_CREATED),
		Code: int32(identity_v1.EventCode_EC_USER),
		SourceType: int32(event_v1.EventSourceType_EST_IDENTITY),
		SourceId: s.serviceId,
		Id: uuid.New().String(),
		ObjectId: &userIdStr,
		UserId: &userIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &user,
	}
	go s.sm.Notify(&e)

	c.JSON(http.StatusCreated, user)
	return
}

func (s *Server) GetUser(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("user_id")); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid UUID for required user_id path parameter"})
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.  Must be admin, have an element role greater than the
	// role binding's role, or be the user itself.
	//
	// NB: the user can always retrieve their own information, even if no
	// roles.
	policies := policy.MakeMatchAdminOrElementRoleOrUserPolicy(policy.RoleOperator, policy.GreaterOrEqual)
	if match, _, _ := s.CheckPolicyMiddlewareExt(tok, &id, tok.GetElementIds(), policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Return if exists.
	var user store.User
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RoleBindings.Role").Preload("PrimaryEmailAddress").Preload("EmailAddresses").Preload("IdpIdentities")
	}
	res := q.First(&user, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		user.SortRoleBindings()
		c.JSON(http.StatusOK, user)
	}
}

func (s *Server) UpdateUser(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("user_id")); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid UUID for required user_id path parameter"})
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var um store.User
	if err := c.ShouldBindJSON(&um); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	var match bool
	var policyName string
	policies := policy.MakeMatchAdminOrUserRolePolicy(policy.RoleAccount, policy.GreaterOrEqual)
	if match, policyName = s.CheckPolicyMiddleware(tok, &id, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var user store.User
	if res := s.db.First(&user, id); res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Only allow admins to *change* the Enabled field.
	if um.Enabled != user.Enabled && (policyName != policy.MatchAdmin) {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized to modify enabled field"})
		return
	}

	// Update allowed fields if exists; do additional input validation along
	// the way.  We allow updates to Name, GivenName, FamilyName, FullName,
	// Password, PrimaryEmailAddressId.  Admins can also update Enabled.
	updates := make(map[string]interface{})
	if um.Name != user.Name {
		if um.Name == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid user name"})
			return
		}
		res := s.db.Where("name = ? and deleted_at is not NULL", um.Name).Where("users.id != ?", id.String()).First(&store.User{})
		if res.RowsAffected > 0 {
			c.JSON(http.StatusBadRequest, gin.H{"error": "user name unavailable"})
			return
		}
		updates["Name"] = um.Name
	}
	if um.FullName != user.FullName {
		updates["FullName"] = um.FullName
	}
	if um.GivenName != user.GivenName {
		updates["GivenName"] = um.GivenName
	}
	if um.FamilyName != user.FamilyName {
		updates["FamilyName"] = um.FamilyName
	}
	if len(um.Password) > 0 {
		if len(user.Password) > 0 {
			if authString := c.GetHeader("Authorization"); authString == "" {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid password confirmation"})
				return
			} else if matches := s.basicAuthRegexp.FindAllStringSubmatch(authString, -1); len(matches) != 1 {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid password confirmation authorization header"})
				return
			} else {
				authPass := matches[0][1]
				decodedConfPass := make([]byte, base64.StdEncoding.DecodedLen(len(authPass)))
				if n, err := base64.StdEncoding.Decode(decodedConfPass, []byte(authPass)); err != nil {
					c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid password confirmation base64 password"})
					return
				} else {
					decodedConfPass = decodedConfPass[:n]
				}
				if err := bcrypt.CompareHashAndPassword(user.Password, decodedConfPass); err != nil {
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "incorrect confirmation password"})
					return
				}
			}
		}

		passwordHash, hperr := bcrypt.GenerateFromPassword([]byte(um.Password), bcrypt.DefaultCost)
		if hperr != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid pasword"})
			return
		} else {
			updates["Password"] = passwordHash
		}
	}
	updates["Enabled"] = um.Enabled
	if um.PrimaryEmailAddressId != nil {
		if res := s.db.Where(&store.UserEmailAddress{Id: *um.PrimaryEmailAddressId, UserId: id}).First(&store.UserEmailAddress{}); res.RowsAffected != 1 {
			c.JSON(http.StatusBadRequest, gin.H{"error": "primary_email_address_id not found"})
			return
		} else {
			updates["PrimaryEmailAddressId"] = *um.PrimaryEmailAddressId
		}
	}

	s.db.Model(&user).Updates(updates)

	q := s.db.Where(&store.User{Id: id})
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RoleBindings.Role").Preload("PrimaryEmailAddress").Preload("EmailAddresses").Preload("IdpIdentities")
	}
	res := q.First(&user)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	user.SortRoleBindings()

	// Generate an event.
	userIdStr := user.Id.String()
	eh := subscription.EventHeader{
		Type: int32(event_v1.EventType_ET_UPDATED),
		Code: int32(identity_v1.EventCode_EC_USER),
		SourceType: int32(event_v1.EventSourceType_EST_IDENTITY),
		SourceId: s.serviceId,
		Id: uuid.New().String(),
		ObjectId: &userIdStr,
		UserId: &userIdStr,
	}
	e := subscription.Event{
		Header: eh,
		Object: &user,
	}
	go s.sm.Notify(&e)

	c.JSON(http.StatusOK, user)
	return
}

func (s *Server) DeleteUser(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("user_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Load the user.  NB: load the rolebindings so we can do more checks
	// below; determine if there is a single element-bound rolebinding; if
	// there is more than one, only admins can delete this user.  It would
	// be better to only do the preload in the non-admin case, but the
	// policy middleware is unhelpful, so just waste a bit.
	var user store.User
	res := s.db.Preload("RoleBindings.Role").First(&user, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	var singleElementId *uuid.UUID
	var singleElementRoleValue int
	for _, rb := range user.RoleBindings {
		if rb.ElementId != nil && rb.ApprovedAt != nil && rb.DeletedAt == nil {
			if singleElementId == nil {
				singleElementId = rb.ElementId
				singleElementRoleValue = rb.Role.Value
			} else {
				singleElementId = nil
				break
			}
		}
	}

	// Check policy.  Admins can delete any user; Operator users and up can
	// delete a user iff the user has only a single lower-value Role in one
	// of the calling user's elements.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	var match bool
	var policyName string
	var tokenRoleBinding *store.RoleBinding
	var elementIds []*uuid.UUID
	if singleElementId != nil {
		elementIds = []*uuid.UUID{singleElementId}
	}
	if match, policyName, tokenRoleBinding = s.CheckPolicyMiddlewareExt(tok, nil, elementIds, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Handle non-admin user deletion cases.
	if policyName != policy.MatchAdmin {
		// Ensure that target User has a single element-bound role; that all
		// unbound roles are lower Role value than caller role in element.
		if singleElementId == nil {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
			return
		} else {
			// Ensure that target user role in element is less than the
			// caller's role in this element:
			if singleElementRoleValue >= tokenRoleBinding.Role.Value {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (insufficient role level)"})
				return
			}

			for _, urb := range user.RoleBindings {
				// Only check valid role bindings.
				if urb.ApprovedAt == nil || urb.DeletedAt != nil {
					continue
				}
				if urb.ElementId != nil {
					// Double-check that the single element the target user
					// is a member of really is the one the caller matched
					// upon.
					if *urb.ElementId != *tokenRoleBinding.ElementId {
						c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "unauthorized"})
						return
					}
					continue
				} else if urb.Role.Value >= tokenRoleBinding.Role.Value {
					// If the target user has an unbound role whose value is
					// greater than the non-admin caller, we cannot allow
					// the deletion.
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (target user unbound higher role)"})
					return
				}
			}
		}
	}

	// Delete user token_role_bindings and tokens.
	var userRoleBindingIds []uuid.UUID
	for _, urb := range user.RoleBindings {
		userRoleBindingIds = append(userRoleBindingIds, urb.Id)
	}
	s.db.Where("role_binding_id in ?", userRoleBindingIds).Delete(&store.TokenRoleBinding{})
	s.db.Where("user_id = ?", id).Delete(&store.Token{})
	t := time.Now().UTC()
	s.db.Model(&store.RoleBinding{}).Where("user_id = ?", id).Update("deleted_at", &t)
	user.DeletedAt = &t
	user.Enabled = false
	s.db.Save(&user)

	c.JSON(http.StatusOK, &user)
}

func (s *Server) ListUserRoleBindings(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("user_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrUserRolePolicy(policy.RoleAccount, policy.GreaterOrEqual)
	if match, _ := s.CheckPolicyMiddleware(tok, &id, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Return if exists.
	var user store.User
	q := s.db.Where(&store.User{Id: id})
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RoleBindings.Role")
	} else {
		q = q.Preload("RoleBindings")
	}
	res := q.First(&user)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		user.SortRoleBindings()
		c.JSON(http.StatusOK, gin.H{"rolebindings": user.RoleBindings})
	}
}

func (s *Server) ListUserElements(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("user_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrUserRolePolicy(policy.RoleAccount, policy.GreaterOrEqual)
	if match, _ := s.CheckPolicyMiddleware(tok, &id, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Return if exists.
	var user store.User
	q := s.db.Where(&store.User{Id: id})
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RoleBindings.Role")
	} else {
		q = q.Preload("RoleBindings")
	}
	res := q.First(&user)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	user.SortRoleBindings()

	var elements []store.Element = make([]store.Element, 0)
	for _, rb := range user.RoleBindings {
		if rb.ElementId == nil {
			continue
		}
		var element store.Element
		res2 := s.db.First(&element, rb.ElementId)
		if res2.RowsAffected != 1 {
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
		elements = append(elements, element)
	}
	c.JSON(http.StatusOK, gin.H{"elements": elements})
}

func (s *Server) ListUserTokens(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("user_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrUserRolePolicy(policy.RoleAccount, policy.GreaterOrEqual)
	if match, _ := s.CheckPolicyMiddleware(tok, &id, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Return if exists.
	var tokens []store.Token = make([]store.Token, 0)
	q := s.db.Where(&store.Token{UserId: id})
	q = q.Where("Tokens.token not like 'rpu_%'")
	q = q.Where(s.db.Where("Tokens.expires_at is NULL").Or("Tokens.expires_at > now()"))
	if c.GetBool("zms.elaborate") {
		q = q.Preload("RoleBindings.RoleBinding.Role")
	} else {
		q = q.Preload("RoleBindings")
	}
	res := q.Find(&tokens)
	if res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{"tokens": tokens})
	}
}

func (s *Server) ListRoleBindings(c *gin.Context) {
	// Check optional filter query parameters.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		} else {
			elementId = &idParsed
		}
	}
	var elementName string
	if p, exists := c.GetQuery("element_name"); exists {
		if p == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		} else {
			elementName = p
		}
	}
	var userFilter string
	if p, exists := c.GetQuery("user"); exists {
		if p == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		} else {
			userFilter = p
		}
	}
	deleted := false
	if p, exists := c.GetQuery("deleted"); exists {
		if v, err := strconv.ParseBool(p); err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		} else {
			deleted = v
		}
	}
	approved := true
	if p, exists := c.GetQuery("approved"); exists {
		if v, err := strconv.ParseBool(p); err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		} else {
			approved = v
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleManager, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if policyName != policy.MatchAdmin && elementId == nil && elementName == "" {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized (must provide authorized element id or name filter when non-admin)"})
		return
	}

	var roleBindings []store.RoleBinding
	q := s.db
	if elementId != nil || elementName != "" {
		if elementId != nil {
			q = q.Where("element_id = ?", *elementId)
			// gorm clearly cannot apply struct queries to joined tables.
			//&store.RoleBinding{ElementId: elementId, DeletedAt: nil})
		} else {
			q = q.Joins("Element").Where("Element.name = ?", elementName)
		}
	}
	if !deleted {
		q = q.Where("deleted_at is NULL")
	} else {
		q = q.Where("deleted_at is not NULL")
	}
	if !approved {
		q = q.Where("approved_at is NULL")
	} else {
		q = q.Where("approved_at is not NULL")
	}
	if userFilter != "" {
		q = q.Joins("User").Where(s.db.Where(store.MakeILikeClause(s.config, "Users.name"), fmt.Sprintf("%%%s%%", userFilter)).Or(store.MakeILikeClause(s.config, "Users.full_name"), fmt.Sprintf("%%%s%%", userFilter)))
	}
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Role")
	}
	q = q.Order("created_at desc")

	var total int64
	q.Model(&store.RoleBinding{}).Count(&total)
	q.Scopes(store.Paginate(page, itemsPerPage)).Find(&roleBindings)

	c.JSON(http.StatusOK, gin.H{"role_bindings": roleBindings, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

type CreateRoleBindingModel struct {
	UserId      uuid.UUID  `json:"user_id" binding:"required"`
	RoleId      uuid.UUID  `json:"role_id" binding:"required"`
	ElementId   *uuid.UUID `json:"element_id"`
	ElementName *string    `json:"element_name"`
	Approved    *bool      `json:"approved"`
}

func (s *Server) CreateRoleBinding(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var cm CreateRoleBindingModel
	if err := c.ShouldBindJSON(&cm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var element store.Element
	if cm.ElementId != nil {
		res := s.db.Where(&store.Element{Id: *cm.ElementId}).First(&element)
		if res.RowsAffected != 1 {
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
	} else if cm.ElementName != nil {
		res := s.db.Where(&store.Element{Name: *cm.ElementName}).First(&element)
		if res.RowsAffected != 1 {
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
	}
	var elementId *uuid.UUID
	if element.Id != uuid.Nil {
		elementId = &element.Id
	}

	var role store.Role
	res := s.db.Where(&store.Role{Id: cm.RoleId}).First(&role)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.  Admin, >=Operator, or any User with >= Account roles
	// can CreateRoleBinding in any non-nil element that exists.  Only
	// Admins can create Admin RoleBindings.
	//
	// If the caller sets Approved==true:
	// * Admins can do anything.
	// * >= Operator can Approve a RoleBinding that is < calling User's
	// RoleBinding in the target elementId.
	var match bool
	var policyName string
	var tokenRoleBinding *store.RoleBinding
	var elementIds []*uuid.UUID
	if elementId != nil {
		elementIds = []*uuid.UUID{elementId}
	}
	policies := policy.MakeMatchAdminOrElementRoleOrUserElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual, policy.RoleAccount, policy.GreaterOrEqual)
	if match, policyName, tokenRoleBinding = s.CheckPolicyMiddlewareExt(tok, &cm.UserId, elementIds, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional input validation.
	if role.Value == policy.RoleAdmin && policyName != policy.MatchAdmin {
		c.JSON(http.StatusBadRequest, gin.H{"error": "only admins can create admin rolebindings"})
		return
	}

	if policyName != policy.MatchAdmin && (cm.ElementId == nil || cm.ElementName == nil) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "must provide element_id or element_name"})
		return
	}

	if cm.Approved != nil && *cm.Approved && policyName != policy.MatchAdmin {
		// >= Operator can Approve a RoleBinding that is < calling User's
		// RoleBinding in the target elementId.
		if tokenRoleBinding.Role.Value < policy.RoleOperator {
			c.JSON(http.StatusBadRequest, gin.H{"error": "caller token role in target element_id is not authorized to auto-approve this rolebinding"})
			return
		} else if tokenRoleBinding.Role.Value <= role.Value {
			c.JSON(http.StatusBadRequest, gin.H{"error": "caller token role in target element_id is not authorized to auto-approve this rolebinding (greater or equal)"})
			return
		}
	}

	rb := store.RoleBinding{
		Id:        uuid.New(),
		Role:      &role,
		UserId:    cm.UserId,
		CreatedAt: time.Now().UTC(),
	}
	if element.Id != uuid.Nil {
		rb.ElementId = &element.Id
	}
	if cm.Approved != nil && *cm.Approved {
		t := time.Now().UTC()
		rb.ApprovedAt = &t
	}
	s.db.Create(&rb)
	c.JSON(http.StatusCreated, rb)
}

func (s *Server) UpdateRoleBinding(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("rolebinding_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var um store.RoleBinding
	if err := c.ShouldBindJSON(&um); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check existence of requested RoleBinding.
	var rb store.RoleBinding
	if res := s.db.Where(&store.RoleBinding{Id: id}).Preload("Role").First(&rb); res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	if rb.ApprovedAt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "role_binding already approved; cannot modify"})
		return
	}

	// Check existence of set RoleId.
	var updatedRole store.Role
	if res := s.db.Where(&store.Role{Id: um.RoleId}).First(&updatedRole); res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.  Only admins, or manager/owner in target element if
	// specified (if requested role is Owner, only another Owner can
	// approve, so is conditional onrequested role.Value), can approve a
	// rolebinding.
	var match bool
	var tokenRoleBinding *store.RoleBinding
	var policies []policy.Policy
	if rb.ElementId != nil {
		rv := rb.Role.Value
		if policy.RoleManager > rv {
			rv = policy.RoleManager
		}
		policies = policy.MakeMatchAdminOrElementRolePolicy(rv, policy.GreaterOrEqual)
	} else {
		policies = policy.AdminOnlyPolicy
	}
	var elementIds []*uuid.UUID
	if rb.ElementId != nil {
		elementIds = []*uuid.UUID{rb.ElementId}
	}
	if match, _, tokenRoleBinding = s.CheckPolicyMiddlewareExt(tok, &id, elementIds, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional input validation.
	if um.ApprovedAt == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "approved_at must be specified"})
		return
	}
	// Now that we have completed the policy check of the current
	// RoleBinding role value against the caller, and are satisfied that
	// they can approve it, if they also changed it, make sure it is to a
	// value <= their role.
	if um.RoleId != rb.RoleId && updatedRole.Value > tokenRoleBinding.Role.Value {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "approved role must be less than or equal to caller role"})
		return
	}

	rb.RoleId = um.RoleId
	rb.ApprovedAt = um.ApprovedAt
	s.db.Save(&rb)

	c.AbortWithStatusJSON(http.StatusOK, rb)
	return
}

func (s *Server) DeleteRoleBinding(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("rolebinding_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Find target RoleBinding.
	var roleBinding store.RoleBinding
	res := s.db.Where(&store.RoleBinding{Id: id}).Preload("Role").First(&roleBinding)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.  Must be admin, user associated with the role binding,
	// or have an element role greater than the role binding's role.
	policies := policy.MakeMatchAdminOrElementRoleOrUserPolicy(roleBinding.Role.Value, policy.Greater)
	var match bool
	if match, _ = s.CheckPolicyMiddleware(tok, &roleBinding.UserId, roleBinding.ElementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	s.db.Where(&store.TokenRoleBinding{RoleBindingId: roleBinding.Id}).Delete(&store.TokenRoleBinding{})
	t := time.Now().UTC()
	roleBinding.DeletedAt = &t
	s.db.Save(&roleBinding)
	c.Status(http.StatusOK)
}

type ListElementsQueryParams struct {
	Element *string `form:"element" binding:"omitempty"`
	Enabled *bool   `form:"enabled" binding:"omitempty"`
	Deleted *bool   `form:"deleted" binding:"omitempty"`
	Sort    *string `form:"sort" binding:"omitempty,oneof=name html_url created_at approved_at updated_at deleted_at enabled"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListElements(c *gin.Context) {
	// Check optional filter query parameters.
	params := ListElementsQueryParams{}
	if err := c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := []policy.Policy{
		policy.Policy{Name: policy.MatchAdmin, Role: policy.RoleAdmin, RoleComparator: policy.GreaterOrEqual},
		policy.Policy{Name: "viewer", Role: policy.RoleViewer, RoleComparator: policy.GreaterOrEqual},
	}
	var match bool
	var policyName string
	match, policyName = s.CheckPolicyMiddleware(tok, nil, nil, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var elements []store.Element
	q := s.db
	if policyName != policy.MatchAdmin {
		sq := s.db.
			Where("Elements.is_public is true").
			Or("Elements.id in ?", tok.GetElementIds())
		q = q.Where(sq)
	}
	if params.Deleted == nil || !*params.Deleted {
		q = q.Where("Elements.deleted_at is NULL")
	} else {
		q = q.Where("Elements.deleted_at is not NULL")
	}
	if params.Enabled != nil {
		if *params.Enabled {
			q = q.Where("Elements.enabled is true")
		} else {
			q = q.Where("Elements.enabled is false")
		}
	}
	if params.Element != nil {
		fs := fmt.Sprintf("%%%s%%", *params.Element)
		q = q.Where(
			s.db.Where(store.MakeILikeClause(s.config, "name"), fs).
				Or(store.MakeILikeClause(s.config, "html_url"), fs).
				Or(store.MakeILikeClause(s.config, "description"), fs))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order(*params.Sort + " " + sortDir)
	} else {
		q = q.Order("updated_at " + sortDir)
	}
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Attributes")
	}

	var total int64
	q.Model(&store.Element{}).Count(&total)
	q.Scopes(store.Paginate(page, itemsPerPage)).Find(&elements)

	c.JSON(http.StatusOK, gin.H{"elements": elements, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) CreateElement(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var user store.User
	if res := s.db.Where(&store.User{Id: tok.UserId}).First(&user); res.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	}

	// Initial input validation.
	var cm store.Element
	if err := c.ShouldBindJSON(&cm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check policy.
	policies := []policy.Policy{
		policy.Policy{Name: policy.MatchAdmin, Role: policy.RoleAdmin, RoleComparator: policy.GreaterOrEqual},
		policy.Policy{Name: "match-account", Role: policy.RoleAccount, RoleComparator: policy.GreaterOrEqual},
	}
	match, policyName := s.CheckPolicyMiddleware(tok, nil, nil, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Additional input validation based on authorization.
	res := s.db.Where(&store.Element{Name: cm.Name}).First(&store.Element{})
	if res.RowsAffected > 0 {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"error": "element already exists"})
		return
	}
	if cm.ApprovedAt != nil && policyName != policy.MatchAdmin {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if cm.Enabled && policyName != policy.MatchAdmin {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if cm.ApprovedAt == nil && cm.Enabled {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must approve element before enabling"})
		return
	}

	t := time.Now().UTC()
	var ownerRole store.Role
	if res = s.db.Where(&store.Role{Name: "owner"}).First(&ownerRole); res.RowsAffected != 1 {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error":"no owner role"})
		return
	}

	element := store.Element{
		Id: uuid.New(), CreatorUserId: tok.UserId, Name: cm.Name, HtmlUrl: cm.HtmlUrl,
		Description: cm.Description, IsPublic: cm.IsPublic,
	}
	if cm.ApprovedAt != nil {
		element.ApprovedAt = cm.ApprovedAt
	}
	if cm.ApprovedAt != nil && cm.Enabled {
		element.Enabled = true
	}
	s.db.Create(&element)

	// Give the creator an owner role.
	s.db.Create(&store.RoleBinding{
		Id: uuid.New(), RoleId: ownerRole.Id, UserId: tok.UserId,
		ElementId: &element.Id, ApprovedAt: &t,
	})

	// If requested, return an "upgraded" default token with the new
	// rolebinding.  Frontend needs this to avoid requiring a new login to
	// fetch a new default token.
	updateTokenString := c.GetHeader("X-Api-Token-Update")
	if updateTokenString == "1" || updateTokenString == "true" || updateTokenString == "True" {
		defTrue := true
		if token, err := s.CreateDefaultTokenForUser(&user, nil, &defTrue); err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": fmt.Errorf("failed to update token with new element rolebinding: %+v", err.Error())})
			return
		} else {
			c.Writer.Header().Set("X-Api-Token", token.Token)
		}
	}

	c.JSON(http.StatusCreated, element)
}

func (s *Server) GetElement(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("element_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// We need to check the IsPublic access bit in this type prior
	// to checking authorization policy.
	var element store.Element
	res := s.db.Where(&store.Element{Id: id}).First(&element)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Check policy.
	policies := []policy.Policy{
		policy.Policy{Name: policy.MatchAdmin, Role: policy.RoleAdmin, RoleComparator: policy.GreaterOrEqual},
		policy.Policy{Name: "match-element-manager", Role: policy.RoleManager, RoleComparator: policy.GreaterOrEqual, ElementComparator: policy.Match},
		policy.Policy{Name: "match-element-viewer", Role: policy.RoleViewer, RoleComparator: policy.GreaterOrEqual, ElementComparator: policy.Match},
		policy.Policy{Name: "viewer", Role: policy.RoleViewer, RoleComparator: policy.GreaterOrEqual},
	}
	match, policyName := s.CheckPolicyMiddleware(tok, nil, &id, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	if !element.IsPublic && policyName == "viewer" {
		// Mere viewers cannot see this element.
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// XXX: eventually we should sanitize things like Endpoint into an
	// ElementEndpoint table.

	// Return if exists.
	q := s.db.Where(&store.Element{Id: id})
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Attributes")
	}
	res = q.First(&element)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, element)
	}
}

func (s *Server) UpdateElement(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("element_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Initial input validation.
	var em store.Element
	if err := c.ShouldBindJSON(&em); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOwner, policy.GreaterOrEqual)
	match, policyName := s.CheckPolicyMiddleware(tok, nil, &id, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var element store.Element
	if res := s.db.Where(&store.Element{Id: id}).First(&element); res.RowsAffected < 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// Only allow admins to *change* the Enabled field for now.
	if em.Enabled != element.Enabled && policyName != policy.MatchAdmin {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized to modify enabled field"})
		return
	}
	if em.ApprovedAt != nil && (element.ApprovedAt == nil || (*em.ApprovedAt != *element.ApprovedAt)) && policyName != policy.MatchAdmin {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized to modify approved_at"})
		return
	}
	if element.ApprovedAt == nil && em.Enabled {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "must approve element before enabling"})
		return
	}

	// Update allowed fields if exists; do additional input validation along
	// the way.
	updates := make(map[string]interface{})
	if em.Name != element.Name {
		if em.Name == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid element name"})
			return
		}
		res := s.db.Where(&store.Element{Name: em.Name}).Where("elements.id != ? and elements.deleted_at is NULL", id.String()).First(&store.Element{})
		if res.RowsAffected > 0 {
			c.JSON(http.StatusBadRequest, gin.H{"error": "element name unavailable"})
			return
		}
		updates["Name"] = em.Name
	}
	if em.HtmlUrl != element.HtmlUrl {
		updates["HtmlUrl"] = em.HtmlUrl
	}
	if em.Description != nil && element.Description != nil && *em.Description != *element.Description {
		updates["Description"] = *em.Description
	}
	if em.IsPublic != element.IsPublic {
		updates["IsPublic"] = em.IsPublic
	}
	if em.Enabled != element.Enabled {
		updates["Enabled"] = em.Enabled
	}
	if em.ApprovedAt != nil && (element.ApprovedAt == nil || (*em.ApprovedAt != *element.ApprovedAt)) {
		updates["ApprovedAt"] = em.ApprovedAt
	}

	s.db.Model(&element).Updates(updates)

	q := s.db.Where(&store.Element{Id: id})
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Attributes")
	}
	res := q.First(&element)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, element)
	}
}

func (s *Server) DeleteElement(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("element_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOwner, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, &id, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Delete if exists.
	var element store.Element
	res := s.db.Where(&store.Element{Id: id}).First(&element)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	t := time.Now().UTC()
	// XXX: should also delete tokens that reference these rolebindings
	s.db.Save(&store.RoleBinding{ElementId: &id, DeletedAt: &t})
	element.DeletedAt = &t
	element.Enabled = false
	s.db.Save(&element)

	c.AbortWithStatusJSON(http.StatusOK, element)
}

type ListServicesQueryParams struct {
	Sort    *string `form:"sort" binding:"omitempty,oneof=name kind created_at updated_at enabled"`
	SortAsc *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) ListServices(c *gin.Context) {
	// Check optional filter query parameters.
	params := ListServicesQueryParams{}
	if err := c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	if _, err := s.RequireContextToken(c); err != nil {
		return
	}

	var services []store.Service
	q := s.db
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order(*params.Sort + " " + sortDir)
	} else {
		q = q.Order("updated_at " + sortDir)
	}

	var total int64
	q.Model(&store.Service{}).Count(&total)
	q.Scopes(store.Paginate(page, itemsPerPage)).Find(&services)

	c.JSON(http.StatusOK, gin.H{"services": services, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) GetRoles(c *gin.Context) {
	// Ensure caller provided a valid token.
	if _, err := s.RequireContextToken(c); err != nil {
		return
	}

	var roles []store.Role = make([]store.Role, 0)
	s.db.Find(&roles)
	c.JSON(http.StatusOK, gin.H{"roles": roles})
}

type CreateSubscriptionModel struct {
	Id            string                     `json:"id" binding:"required,uuid"`
	Filters       []subscription.EventFilter `json:"filters" binding:"omitempty"`
}

func (s *Server) CreateSubscription(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm CreateSubscriptionModel
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// NB: for now, non-admins will be restricted to self-user-associated
	// events.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {

		// Build a map of elementId string to (max) role value in that
		// element.
		// Also build a list of all ElementIds in the token.
		// Also build a list of ElementIds with role >= Operator.
		elementRoleMap := make(map[string]int)
		elementIds := make([]string, 0, len(tok.RoleBindings))
		opElementIds := make([]string, 0)
		for _, rb := range tok.RoleBindings {
			if rb.RoleBinding == nil || rb.RoleBinding.ElementId == nil || rb.RoleBinding.Role == nil {
				continue
			}
			eis := rb.RoleBinding.ElementId.String()
			nv := rb.RoleBinding.Role.Value
			if v, ok := elementRoleMap[eis]; ok {
				if nv > v {
					elementRoleMap[eis] = nv
				}
			} else {
				elementRoleMap[eis] = nv
			}
			elementIds = append(elementIds, eis)
			if nv >= policy.RoleOperator {
				opElementIds = append(opElementIds, eis)
			}
		}
		userIds := []string{tok.UserId.String()}

		//
		// Each filter must match these constraint checks:
		//   * have UserIds set to exactly the calling user, and must
		//     have ElementIds set to a subset of the ElementIds in the token; OR
		//   * have UserIds set to exactly the calling user, and ElementIds nil; OR
		//   * have UserIds set to nil, and ElementIds set to elements in which the user has >= Operator role.
		//
		if dm.Filters == nil || len(dm.Filters) < 1 {
			if false {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must provide at least one filter, with UserIds set to exactly their user ID, and ElementIds to a subset of the calling token's element IDs."})
				return
			} else {
				dm.Filters = []subscription.EventFilter{
					subscription.EventFilter{UserIds: userIds},
					subscription.EventFilter{UserIds: userIds, ElementIds: elementIds},
				}
				if len(opElementIds) > 0 {
					dm.Filters = append(dm.Filters, subscription.EventFilter{ElementIds: opElementIds})
				}
				log.Debug().Msg(fmt.Sprintf("autocreated user subscription %+v for token %+v", dm, tok))
			}
		}

		// Filters must be bound to calling user unless user has >= operator
		// role in the associated elements.
		for _, filter := range dm.Filters {
			if filter.UserIds == nil || len(filter.UserIds) == 0 {
				// In this case, the user must provide at least one element
				// in which they have >= Operator role.
				if len(opElementIds) == 0 {
					msg := "non-admin, non-operator token subscriptions must set UserIds to exactly their token UserId"
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else if filter.ElementIds == nil || len(filter.ElementIds) == 0 {
					msg := "token subscriptions without UserIds must set ElementIds to at least one Element in which they have >= Operator role."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else {
					for _, eis := range filter.ElementIds {
						if rv, ok := elementRoleMap[eis]; !ok || rv < policy.RoleOperator {
							msg := fmt.Sprintf("non-admin, wildcard user filter cannot include a sub-Operator role (element %s)", eis)
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			} else {
				if len(filter.UserIds) != 1 || filter.UserIds[0] != tok.UserId.String() {
					msg := "non-admin subscription must set each filter.UserIds field to a one-item list containing exactly the calling token UserId."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				}
				if filter.ElementIds == nil || len(filter.ElementIds) < 1 {
					//msg := "non-admin subscription must set each filter.ElementIds field to a subset of the ElementIds in the calling token."
					//c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					//return
				} else {
					for _, elementId := range filter.ElementIds {
						if _, ok := elementRoleMap[elementId]; !ok {
							msg := "non-admin user must set each filter's ElementIds field to a subset of the ElementIds in the calling token."
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			}
		}
	}

	if sub, err := s.sm.Subscribe(dm.Id, dm.Filters, nil, c.ClientIP(), subscription.Rest, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	} else {
		c.JSON(http.StatusCreated, sub)
	}

	return
}

func (s *Server) ListSubscriptions(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"subscriptions": s.sm.GetSubscriptions()})
	return
}

func (s *Server) GetSubscriptionEvents(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	var subprotos []string
	if xApiTok := c.GetHeader("X-Api-Token"); xApiTok != "" {
		if tok, err = s.RequireContextToken(c); err != nil {
			return
		}
	} else if wsProto := c.GetHeader("Sec-WebSocket-Protocol"); wsProto != "" {
		var wsTok store.Token
		if code, altErr := s.CheckTokenValue(wsProto, &wsTok); altErr != nil {
			log.Warn().Msg(fmt.Sprintf("bad token (%s) in Sec-WebSocket-Protocol: %+v", wsProto, altErr.Error()))
			c.AbortWithStatusJSON(code, gin.H{"error": altErr.Error()})
			return
		} else {
			tok = &wsTok
			subprotos = append(subprotos, wsProto)
		}
	}
	if tok == nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "no token provided"})
		return
	} else {
		log.Debug().Msg(fmt.Sprintf("valid token from websocket"))
	}

	ech := make(chan *subscription.Event)
	if err := s.sm.UpdateChannel(idStr, ech, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// See if client wants to reuse subscription upon websocket close.
	deleteOnClose := true
	if tokenString := c.GetHeader("X-Api-Delete-On-Close"); tokenString == "false" {
		c.Set("zms.deleteOnClose", false)
		deleteOnClose = false
	} else {
		c.Set("zms.deleteOnClose", true)
	}
	
	defer func() {
		if deleteOnClose {
			s.sm.Unsubscribe(idStr)
		} else {
			log.Debug().Msg(fmt.Sprintf("retaining subscription %s", idStr))
			s.sm.UpdateChannel(idStr, nil, &tok.Token)
		}
	}()

	wsUpgrader := websocket.Upgrader{
		ReadBufferSize: 0,
		WriteBufferSize: 16384,
		Subprotocols: subprotos,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	var conn *websocket.Conn
	if conn, err = wsUpgrader.Upgrade(c.Writer, c.Request, nil); err != nil {
		log.Debug().Msg(fmt.Sprintf("error upgrading websocket conn: %+v", err.Error()))
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Handle client disconnect.
	cch := make(chan interface{})
	go func() {
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				cch <- nil
				conn.Close()
				return
			}
		}
	}()

	// Read events from SubscriptionManager, and client read errors or
	// client disconnection.
	for {
		select {
		case e := <- ech:
			if body, jErr := json.Marshal(e); jErr != nil {
				log.Error().Err(jErr).Msg(fmt.Sprintf("error marshaling event (%+v): %s", e, err.Error()))
				continue
			} else {
				log.Debug().Msg(fmt.Sprintf("marshaled event: %+v", body))
				if err = conn.WriteMessage(websocket.TextMessage, body); err != nil {
					return
				}
			}
		case <- cch:
			// NB: the deferred handler covers this case.
			return
		}
	}

	return
}

func (s *Server) DeleteSubscription(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *store.Token
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var sub *subscription.Subscription[*subscription.Event]
	if sub = s.sm.GetSubscription(idStr); sub == nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		if sub.Token == nil || *sub.Token != tok.Token {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
			return
		}
	}

	if err := s.sm.Unsubscribe(idStr); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	c.Status(http.StatusOK)

	return
}
