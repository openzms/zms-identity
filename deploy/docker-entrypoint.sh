#!/bin/sh

( cd /usr/local/share/zms-identity/db \
      && /usr/local/share/zms-identity/db/zms-apply-migrations.sh ) \
   || exit $?

exec /usr/local/bin/zms-identity
