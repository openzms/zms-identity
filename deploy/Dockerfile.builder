FROM golang:1.21-bookworm as build

RUN apt-get update -y \
  && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y \
    make git less nano curl ca-certificates \
  && apt-get clean all \
  && rm -rfv /var/lib/apt/lists/*

ENV ATLAS_VERSION=v0.28.1
RUN curl -sSf https://atlasgo.sh | sh -s -- --community
