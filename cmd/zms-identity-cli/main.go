// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/google/subcommands"
	"os"
	"strings"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/token"
)

type tokenGenerateCmd struct {
	Prefix string
}

func (*tokenGenerateCmd) Name() string     { return "token-generate" }
func (*tokenGenerateCmd) Synopsis() string { return "Generate an OpenZMS token." }
func (*tokenGenerateCmd) Usage() string {
	return `token-generate <kind>:

Generate a valid OpenZMS token.

  <kind>  One of user, pat, service.
`
}
func (*tokenGenerateCmd) SetFlags(f *flag.FlagSet) {}
func (tgc *tokenGenerateCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		f.Usage()
		return subcommands.ExitUsageError
	}
	tgc.Prefix = strings.Trim(f.Arg(0), " \t")
	var tt token.TokenType
	switch tgc.Prefix {
	case "user":
		tt = token.UserToken
	case "pat":
		tt = token.PersonalAccessToken
	case "service":
		tt = token.ServiceToken
	default:
		f.Usage()
		return subcommands.ExitUsageError
	}
	if token, err := token.Generate(tt); err == nil {
		fmt.Println(token)
		return subcommands.ExitSuccess
	} else {
		fmt.Println("Error: " + err.Error())
		return subcommands.ExitFailure
	}
}

type tokenValidateCmd struct {
	Token string
}

func (*tokenValidateCmd) Name() string     { return "token-validate" }
func (*tokenValidateCmd) Synopsis() string { return "Validate an OpenZMS token." }
func (*tokenValidateCmd) Usage() string {
	return `token-validate <token>:

Validate an OpenZMS token.
`
}
func (*tokenValidateCmd) SetFlags(f *flag.FlagSet) {}
func (tvc *tokenValidateCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		f.Usage()
		return subcommands.ExitUsageError
	}
	tvc.Token = strings.Trim(f.Arg(0), " \t")
	if tvc.Token == "" {
		f.Usage()
		return subcommands.ExitUsageError
	}
	err := token.Validate(tvc.Token)
	if err == nil {
		return subcommands.ExitSuccess
	} else {
		fmt.Println("Error: " + err.Error())
		return subcommands.ExitFailure
	}
}

func main() {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")
	subcommands.Register(&tokenGenerateCmd{}, "")
	subcommands.Register(&tokenValidateCmd{}, "")
	flag.Parse()
	ctx := context.Background()
	os.Exit(int(subcommands.Execute(ctx)))
}
