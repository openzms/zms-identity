// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/config"
	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/server/northbound"
	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/server/southbound"
	"gitlab.flux.utah.edu/openzms/zms-identity/pkg/store"
)

func main() {
	serverConfig := config.LoadConfig()
	fmt.Printf("serverConfig: %+v\n", serverConfig)

	if serverConfig.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else if serverConfig.Verbose {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	}

	dbConfig := gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	}
	db, err := store.GetDatabase(serverConfig, &dbConfig)
	if err != nil {
		log.Fatal().Msg("failed to connect to database")
	}
	var bootstrapElementId *uuid.UUID
	var bootstrapUserId *uuid.UUID
	bootstrapElementId, bootstrapUserId, err = store.InitDatabase(serverConfig, db)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to initialize database")
	} else {
		log.Debug().Msg(fmt.Sprintf("initialized database: bootstrap element=%v, user=%v", bootstrapElementId, bootstrapUserId))
	}

	sm := subscription.NewSubscriptionManager[*subscription.Event]()

	httpServer, err := northbound.NewServer(serverConfig, db, sm, bootstrapElementId, bootstrapUserId)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to start http server; terminating")
	}
	httpServer.Run()

	grpcServer, err := southbound.NewServer(serverConfig, db, sm, bootstrapElementId, bootstrapUserId)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to start rpc server; terminating")
	}
	grpcServer.Run()

	// Run until signaled
	sigch := make(chan os.Signal)
	signal.Notify(sigch, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigch
	log.Info().Msg(fmt.Sprintf("shutting down server (signal %d)", sig))

	sm.Shutdown()

	if err := httpServer.Shutdown(context.Background()); err != nil {
		log.Error().Err(err).Msg("failed to gracefully stop http server; terminating")
	}

	if err := grpcServer.Shutdown(context.Background()); err != nil {
		log.Error().Err(err).Msg("failed to gracefully stop gRPC server; terminating")
	}

	log.Info().Msg("server shutdown complete")
}
