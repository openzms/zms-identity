<!--
SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# zms-identity (OpenZMS identity service)

The [OpenZMS software](https://gitlab.flux.utah.edu/openzms) is a prototype
automatic spectrum-sharing management system for radio dynamic zones.
OpenZMS provides mechanisms to share electromagnetic (radio-frequency)
spectrum between experimental or test systems and existing spectrum users,
and between multiple experimental systems.  We are building and deploying
OpenZMS within the context of the POWDER wireless testbed in Salt Lake City,
Utah, part of the NSF-sponsored Platforms for Advanced Wireless Research
program, to create [POWDER-RDZ](https://rdz.powderwireless.net).

This repository contains an identity service (users, elements, etc) that
provides role-based access control tokens.

## Quick dev mode start

Build the project:

```bash
make
```

The [zms-identity](https://gitlab.flux.utah.edu/openzms/zms-identity) service
supports direct credential login and verifies IdP single sign-on access
tokens from GitHub, Google, and CILogon, but you probably want to initialize
the identity service database with a known `admin` account password and a
well-known admin token.

Our token format is patterned after the current GitHub model in most
details, so we do *not* encode scope info in the token, but tokens have a
very specific format, so you must generate one if you want to immediately
use the API.  Otherwise, you will have to generate one via the frontend
after you login.

Generate a token:

```bash
export TOKEN=`./build/output/zms-identity-cli token-generate user`
echo $TOKEN
```

In the same shell where you generated the initial token, run the service:

```bash
mkdir -p tmp
./build/output/zms-identity \
    --db-dsn tmp/zms.sqlite \
    --bootstrap --bootstrap-email 'root@localhost' --bootstrap-password 'a-good-password' \
    --bootstrap-token "$TOKEN" --bootstrap-user-element-name powder \
    --bootstrap-user-element-html-url https://www.powderwireless.net/dev/johnsond \
    --bootstrap-user-element-endpoint \
    localhost:3333 -debug
```
